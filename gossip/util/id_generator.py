"""Implementation of message id generation"""
import queue


class IdGenerator(queue.Queue):
    r"""IdGenerator class used to keep track of all message ids"""
    def __init__(self, size):
        r"""Sets up queue with all ids.

        \param  size        size of generator
        """
        super(IdGenerator, self).__init__(maxsize=size)
        # populate queue
        for id in range(size):
            self.put(id)
