"""Implementation of a thread safe dictionary."""
import threading
import socket


class Subscription(list):
    r"""Entry for a subscription dictionary.

    Attributes:
    lock:    threading lock for accessing data
    """

    def __init__(self, *args):
        r"""Entry for a subscription.

        \param  args    casual list arguments
        """
        list.__init__(self, *args)
        self.lock = threading.Lock()

    def __setitem__(self, key, value):
        r"""Item setting.

        \param  key     index of item
        \param  value   contents for item at specified index
        """
        try:
            self.lock.acquire()
            list.__setitem__(self, key, value)
        finally:
            self.lock.release()

    def __getitem__(self, key):
        r"""Item retrieval.

        \param  key     index of item
        """
        try:
            self.lock.acquire()
            return list.__getitem__(self, key)
        finally:
            self.lock.release()

    def append(self, value):
        r"""Item appending.

        \param  value   contents to be appended
        """
        try:
            self.lock.acquire()
            list.append(self, value)
        finally:
            self.lock.release()

    def remove(self, sock: socket.socket):
        r"""Remove item from list.

        \param  sock    socket object to be removed(matches)
        """
        try:
            self.lock.acquire()
            list.remove(self, sock)
        finally:
            self.lock.release()


class Subscriptions(dict):
    r"""Mechanism for handling subscriptions.

    Attributes:
    lock:    threading lock for accessing data
    """

    def __init__(self, *args):
        r"""Setup for subscription dict.

        \param  args    arguments that can be passed to dict
        """
        self.lock = threading.Lock()
        dict.__init__(self, *args)

    def __setitem__(self, key, value: Subscription):
        r"""Thread safe adding to dict.

        \param  key     index of item
        \param  value   Subscription object
        """
        self.lock.acquire()
        try:
            super(Subscriptions, self).__setitem__(key, value)
        finally:
            self.lock.release()

    def __delitem__(self, key):
        r"""Thread safe deletion from dict.

        \param  key     index of item
        """
        self.lock.acquire()
        try:
            super(Subscriptions, self).__delitem__(key)
        finally:
            self.lock.release()

    def setdefault(self, key, value):
        r"""Thread safe setdefault for dict.

        \param  key     index of item
        \param  value   value to insert if item at index does not exist
        """
        self.lock.acquire()
        try:
            return super(Subscriptions, self).setdefault(key, value)
        finally:
            self.lock.release()
