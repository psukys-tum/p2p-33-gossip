r"""Utility functions used in the context of listeners.

\package gossip.listeners
"""
import socket


def create_socket(port: int, addr: str) -> socket.socket:
    r"""Set up the socket with given HOST and PORT.

    \param  port    specific port used in HOST
    \param  addr    specific addr to the HOST

    \return         set up socket object
    """
    sock = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    # to allow for shutdown
    sock.settimeout(2)
    sock.bind((addr, port))
    sock.listen(5)
    return sock
