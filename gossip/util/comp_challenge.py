"""Utility for generating computational challenges."""
import hashlib
import binascii
from Crypto.PublicKey import RSA


def hashsum(data: bytes) -> bytes:
    r"""Get a hashsum for specified data.

    \param  data    binary data
    \return         hashsum for the data
    """
    return hashlib.sha256(repr(data).encode()).digest()


def sign(priv_key: RSA._RSAobj, thing_to_sign: bytes) -> bytes:
    r"""Wrap signing of thing_to_sign with priv_key.

    \param  priv_key            private priv_key for signion
    \param  thing_to_sign       binary data to sign
    \return                     signed data
    """
    # 2nd param is deprecated but needed -_-
    s = priv_key.sign(thing_to_sign, None)[0]
    # https://stackoverflow.com/a/28524760/552214
    hex_string = '%x' % s
    n = len(hex_string)
    return binascii.unhexlify(hex_string.zfill(n + (n & 1)))


def verify(pub_key: RSA._RSAobj, thing_to_verify: bytes,
           expected: bytes) -> bytes:
    r"""Wrap verification of thing_to_verify with pub_key.

    \param  pub_key             public pub_key for verification
    \param  thing_to_verify     binary data for verification
    \param  expected            expected decrypted message
    \return                     verified data
    """
    data = (int.from_bytes(thing_to_verify, byteorder='big'),)
    return pub_key.verify(expected, data)
