"""Utility function for functions that specialize in encoding data."""
import struct


def gossip_notification(data: bytes, id: int, data_type: str):
    r"""Encode data into a GOSSIP_NOTIFICATION message.

    \param data         binary contents of the message
    \param id           unique identifier for the message (non p2p)
    \param data_type    data type that module subscribed to get notification
    \return     encoded binary data
    """
    header_size = 8  # bytes. size, api type, message id, data type 16b each
    target_size = header_size + len(data)
    api_code = 502
    target = struct.pack('!HHHH{0}s'.format(len(data)),
                         target_size,
                         api_code,
                         id,
                         data_type,
                         data)

    return target
