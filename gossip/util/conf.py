r"""Config provider for Gossip.

\package gossip.config
"""
import configparser
import logging
from gossip.util.decoders import decode_pem_with_pub_priv


def parse_ip(ip_port: str):
    r"""Parse ip:port string.

    \param  ip_port ip:port

    \return return  ip and port
    """
    ip, sep, port = ip_port.rpartition(':')
    if not sep:
        raise KeyError('parse_ip')

    return ip, int(port)


class GossipConfig:
    r"""Config reader and provider class.

    Attributes:
    cache_size:          number of cached data items
    max_connections:     max number of connected peers
    bootstrapper_addr:   bootstrap peer ip
    bootstrapper_port:   bootstrap peer port
    is_bootstrapper:     if we're a bootstrapper node, this is True
    listen_addr:         ip addr peers connect to you with
    listen_port:         port peers connect to you with
    api_addr:            ip addr local modules connect to you with
    api_port:            port local modules connect to you with
    """

    def __init__(self, config_file: str="sample.conf"):
        r"""Initialize config.

        \param config_file name of config file
        """
        self.logger = logging.getLogger(self.__class__.__name__)

        config = configparser.ConfigParser()
        config.read(config_file)

        try:
            global_section = config['global']
            hostkey = global_section.get('hostkey')
            gossip = config['gossip']

            cache_size = gossip.getint('cache_size')
            max_conn = gossip.getint('max_connections')
            bootstrapper = gossip.get('bootstrapper')
            listen_addr = gossip.get('listen_address')
            api_addr = gossip.get('api_address')
            num_workers = gossip.get('num_workers')
        except KeyError as err:
            raise KeyError('Error retrieving field {} in config file'.
                           format(str(err)))

        self.cache_size = cache_size
        self.max_connections = max_conn
        addr, port = parse_ip(bootstrapper)
        self.bootstrapper_addr = addr
        self.bootstrapper_port = port
        self.listen_addr, self.listen_port = parse_ip(listen_addr)
        self.is_bootstrapper = (self.listen_addr == self.bootstrapper_addr and
                                self.listen_port == self.bootstrapper_port)
        self.api_addr, self.api_port = parse_ip(api_addr)
        self.num_workers = int(num_workers)
        self.private_key, self.public_key = decode_pem_with_pub_priv(hostkey)

    def __str__(self):
        r"""Print class contents."""
        return str(self.__class__) + ": " + str(self.__dict__)
