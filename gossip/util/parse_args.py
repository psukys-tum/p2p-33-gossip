import argparse


def parse_args() -> str:
    """Parse command-line arguments"""
    parser = argparse.ArgumentParser(
        description='Gossip module of anonymous and unobservable VoIP.')
    parser.add_argument('-c', '--config', action='store', type=str,
                        required=True, help='path of config')

    args = parser.parse_args()
    return args
