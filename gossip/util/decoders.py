"""Utility functions for decoding data."""
from Crypto.PublicKey import RSA
from typing import Tuple


def decode_pem_with_pub_priv(filepath: str) -> Tuple[RSA._RSAobj, RSA._RSAobj]:
    r"""Decodes PEM file into public and private keys.

    \param  filepath        path to PEM file
    \return                 tuple (private_key, public_key,)
    """
    # Read file
    with open(filepath, 'r') as f:
        contents = f.read()

    # Separate public from private:
    # 1. find indices that mark begin&end for both keys
    # 2. extract including marks (begin and end)
    private_marks = {'start': '-----BEGIN RSA PRIVATE KEY-----',
                     'end': '-----END RSA PRIVATE KEY-----'}

    public_marks = {'start': '-----BEGIN PUBLIC KEY-----',
                    'end': '-----END PUBLIC KEY-----'}
    private_start = contents.find(private_marks['start'])
    private_end = contents.find(private_marks['end'], private_start)

    public_start = contents.find(public_marks['start'])
    public_end = contents.find(public_marks['end'], public_start)

    private_str = contents[private_start: private_end +
                           len(private_marks['end'])]
    public_str = contents[public_start: public_end +
                          len(public_marks['end'])]

    private_key = RSA.importKey(private_str)
    public_key = RSA.importKey(public_str)

    return (private_key, public_key,)
