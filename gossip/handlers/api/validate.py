"""Validation API handler."""
from gossip.handlers.handler import Handler
import logging
import struct


class GossipValidate(Handler):
    r"""Validation handler.

    Attributes:
    api_header:     contents of the message
    p2p_output:     output queue for p2p endpoint
    subscriptions:  list of subscribed peers
    notifications:  list of messages that have to be validated
    message_id:     internal identifier for messages
    reserved:       unused space
    valid:          validation flag (valid/invalid)

    """

    code = 503

    def __init__(self, params: dict):
        r"""Construct validation handler.

        \param  params      dictionary of parameters passed from handler
        """
        self.api_header = params['data']
        self.p2p_output = params['p2p_output']
        self.subscriptions = params['subscriptions']
        self.notifications = params['notifications']
        self.sender = params['conn']
        self.brahms_gossip = params['brahms_gossip']
        self.logger = logging.getLogger(self.__class__.__name__)

    def parse(self) -> bool:
        """Parse API header.

        API header specification:
        message ID (16 bits), reserved (15 bits), valid (1 bit)
        """
        # struct smallest unit is bytes - read reserved with valid into on
        try:
            message_id, reserved = struct.unpack('!HH', self.api_header)
        except struct.error as err:
            header_size = struct.calcsize(self.api_header)
            self.logger.error('API header unpack failed:\n \
                               {0}, given {1}'.format(err, header_size))
            return False

        self.message_id = message_id
        # reserved is 8 bits - 7 for reserved, 1 for valid
        self.reserved = reserved >> 1
        self.valid = (reserved & 1) == 1

        return True

    def action(self):
        """Set validation data to specific message."""
        # Check if validator is valid (subscribed):
        # 1. message validation expected
        if self.message_id not in self.notifications:
            self.logger.error(
                'Message ID {0} not in notifications'.format(self.message_id))
            return False

        data_type = self.notifications[self.message_id].data_type

        # 2. data type subscribed
        if data_type not in self.subscriptions:
            self.logger.error(
                'data_type {0} is not subscribed'.format(data_type))
            return False

        subscribers = self.subscriptions[data_type]

        # 3. validation sender in subscription list
        if self.sender not in subscribers:
            self.logger.error('sender is not a subscriber')
            return False

        # Check if flag is valid
        if not self.valid:
            self.logger.error('Message is not valid')
            return False

        # Retrieve peers and queue them with their data
        self.p2p_output.put({'targets': self.brahms_gossip.view[:],
                             'data': self.notifications[self.message_id].data})
        self.notifications.pop(self.message_id)
        return True
