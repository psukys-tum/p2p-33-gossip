r"""Implementation of Gossip Notify."""
import struct
import socket  # type-hinting
import logging

from gossip.util.subscriptions import Subscriptions
from gossip.handlers.handler import Handler


class GossipNotify(Handler):
    r"""Gossip Notify class.

    Attributes:
    data:            data used for parsing notify request
    subscriptions:   dictionary of subscribers
    conn:            socket that provided the data
    """

    code = 501

    def __init__(self, params: dict):
        r"""Init gossip notify.

        \param  params      parameters used by action
        """
        self.logger = logging.getLogger(self.__class__.__name__)
        self.data = params['data']
        self.subscriptions = params['subscriptions']
        self.conn = params['conn']

    def parse(self) -> bool:
        r"""Parse Gossip notify msg from raw data.

        \return             true if parse is successful
        """
        try:
            self._reserved, self.data_type = struct.unpack('!HH', self.data)
        except struct.error as err:
            self.logger.error(
                'GossipNotify parse struct unpack error:\n{0}'.format(err))
            return False
        return True

    def action(self):
        r"""Add notify to subscription list.

        Behavior of action is undefined if parse returns false
        """
        # same connection can't subscribe to same data_type mutliple times
        if self.conn not in self.subscriptions.setdefault(self.data_type, []):
            self.subscriptions.setdefault(
                self.data_type, []).append(self.conn)

    def __str__(self):
        r"""Print class contents."""
        return str(self.__class__) + ": " + str(self.__dict__)
