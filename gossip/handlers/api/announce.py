r"""Implementation of Gossip Announce Handler

\package gossip.api
"""
import struct
from struct import pack, unpack
from gossip.util.subscriptions import Subscriptions
from gossip.handlers.handler import Handler
from gossip.p2p.p2p_pb2 import MessageWrapper
from gossip.util.notifications import Notifications, Notification
from gossip.util.id_generator import IdGenerator
from gossip.p2p.gossip import BrahmsGossip
import logging
import random


class GossipAnnounce(Handler):
    """Gossip Announce Handler class"""
    # Require(size = arbitrary, msg_type = 500)
    code = 500

    def __init__(self, params: dict):
        """Init Gossip announce

        \param params parameters used by action
        """
        self.logger = logging.getLogger(self.__class__.__name__)

        self.api_output = params['api_output']
        self.p2p_output = params['p2p_output']
        self.data = params['data']
        self.subscriptions = params['subscriptions']
        self.notifications = params['notifications']
        self.conn = params['conn']
        self.timeout = params['timeout']
        self.brahms_gossip = params['brahms_gossip']
        self.data_type = 0
        self.ttl = 0
        self.msg_data = None
        self._reserved = 0

    def parse(self):
        r"""Parse Gossip announce msg from raw data."""
        off = 4
        try:
            self.ttl, self._reserved, self.data_type, self.msg_data = unpack(
                '!BBH{0}s'.format(len(self.data) - off), self.data)

        except struct.error as err:
            self.logger.error(
                'GossipAnnounce parse struct unpack error:\n{0}'.format(err))
            return False

        return True

    def action(self):
        r"""Adds announce to output queues."""
        # Try to get subscribed sockets
        try:
            socks = self.subscriptions[self.data_type]
            # Don't echo announces
            try:
                socks.remove(self.conn)
            except ValueError:
                pass
        except KeyError:
            socks = []

        # wait for verification from subs before announce
        # TODO: if the sockets in socks are closed here, we can't get
        # validation and the message will never be sent to peers.
        # Fringe case and announce is best effort, let it slide for now.
        # /Adam
        if socks:
            p2p_data = self._generate_p2p_msg()

            notif = Notification(data=p2p_data,
                                 timeout_length=self.timeout,
                                 origin=self.conn,
                                 data_type=self.data_type)

            msg_id = self.notifications.id_generator.get()
            self.notifications[msg_id] = notif

            api_data = self._generate_api_msg(msg_id)
            if not api_data:
                return

            self.api_output.put(
                {'data': api_data,
                 'targets': self.subscriptions[self.data_type]})
        else:
            # announce directly if no subs to verify
            p2p_data = self._generate_p2p_msg()
            self.p2p_output.put({'data': p2p_data,
                                 'targets': self.brahms_gossip.view[:]})

    def _generate_p2p_msg(self):
        """Generate p2p encoded message"""
        msg = MessageWrapper()
        msg.type = MessageWrapper.DATASPREAD
        msg.ip = self.brahms_gossip.ip
        msg.dataSpread.ttl = self.ttl
        msg.dataSpread.data_type = self.data_type
        msg.dataSpread.data = self.msg_data
        return msg.SerializeToString()

    def _generate_api_msg(self, msg_id: int):
        """Generate api encoded notification message

        \param msg_id message id of notification message
        """
        try:
            msg = pack("!HHHH{0}s".format(len(self.msg_data)),
                       8+len(self.msg_data), 502, msg_id,
                       self.data_type, self.msg_data)
            return msg

        except struct.error as err:
            self.logger.error(
                'GossipAnnounce gen api msg unpack error:\n{0}'.format(err))
            return None

    def __str__(self):
        r"""Print class contents"""
        return str(self.__class__) + ": " + str(self.__dict__)
