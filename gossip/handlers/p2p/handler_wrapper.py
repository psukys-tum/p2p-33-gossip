"""P2P handler wrapper."""
import logging
import inspect  # to insepct module variables
import sys  # to get current module
import struct
from queue import Queue
from gossip.handlers.handler import Handler
from google.protobuf.message import DecodeError
from gossip.p2p.p2p_pb2 import MessageWrapper
from gossip.handlers.p2p import dataspread, hello, idspread


class P2PHandlerWrapper:
    r"""P2P wrapper class that does deduction on which handler to use.

    Attributes:
    logger:     object for logging
    params:     a dictionary of parameters
    handlers:   P2P request handler list

    """

    def __init__(self, params: dict):
        """Setup P2P wrapper handler."""
        self.logger = logging.getLogger(self.__class__.__name__)
        self.params = params
        self.handlers = self.register_handlers()

    def register_handlers(self) -> list:
        r"""Get a list of handlers available.

        \return     a list of classes that are P2P handlers
        """
        # TODO: make an automatic approach for this
        self.handlers = [dataspread.DataSpread,
                         idspread.IdSpread,
                         hello.HelloAuth]
        return self.handlers

    def get_handler(self, code: int) -> Handler:
        r"""Retrieve specific P2P handler by its code.

        \param code     P2P type code (see p2p/p2p.proto)
        \return         Specific handler or None
        """
        for handler in self.handlers:
            if handler.code == code:
                return handler

    def handle_message(self, message: bytes) -> bool:
        r"""Handle a raw message

        \param message      raw data that likely contains headers and data
        \return             whether handling was successful
        """
        message_wrapper = MessageWrapper()
        try:
            message_wrapper.ParseFromString(message)
        except DecodeError as exc:
            self.logger.error(
                'Handle message ParseFromString error:\n{0}'.format(exc))
            return False

        handler_class = self.get_handler(message_wrapper.type)
        if not handler_class:
            self.logger.error('No handler classes for {0}\
             p2p code found'.format(message_wrapper.type))
            return False

        self.params['message_wrapper'] = message_wrapper

        handler = handler_class(self.params)

        parsed = handler.parse()
        if parsed:
            return handler.action()
        else:
            self.logger.error('Handle message failed to parse.')
            return False
