"""HELLOAUTH handler."""
import logging
from gossip.handlers.handler import Handler
from gossip.p2p.p2p_pb2 import MessageWrapper as MW
from gossip.p2p.p2p_pb2 import HelloAuth as HA
from google.protobuf.message import DecodeError


class HelloAuth(Handler):
    r"""HELLOAUTH handler.

    Attributes:
    message_wrapper:    protobuf generated Python class from protobuf spec
    logger:             object for logging
    hello_auth:         object for HelloAuth protobuf datatype
    p2p_output:         output queue for p2p outputter

    """

    code = MW.HELLOAUTH

    def __init__(self, params: dict):
        r"""Construct helloauth handler.

        \param  params      dictionary of parameters passed from handlerwrapper
        """
        self.message_wrapper = params['message_wrapper']
        self.my_ip = params['my_ip']
        self.socket = params['conn']
        self.hello_auth = HA()
        self.p2p_output = params['p2p_output']
        self.logger = logging.getLogger(self.__class__.__name__)

    def parse(self) -> bool:
        r"""Parse message.

        \return (bool) check if message's format is right"""
        if not self.message_wrapper.HasField('helloAuth'):
            self.logger.error(
                'Missing helloAuth field')
            return False
        else:
            self.hello_auth = self.message_wrapper.helloAuth

        return self.hello_auth.const in [HA.REQUEST,
                                         HA.RESPONSE]

    def action(self):
        """Process the mesasge. Either send response or fulfill response."""
        response = MW()
        if self.hello_auth.const is HA.REQUEST:
            response.type = MW.HELLOAUTH
            response.ip = self.my_ip
            response.helloAuth.const = HA.RESPONSE
        elif self.hello_auth.const is HA.RESPONSE:
            # Current Gossip handles responses itself
            return False
        else:
            self.logger.error('Unknown helloAuth type')
            return False

        self.p2p_output.put({'targets': [self.socket],
                             'data': response.SerializeToString()})
        return True
