"""IDSPREAD handler."""
import logging
from gossip.handlers.handler import Handler
from gossip.p2p.p2p_pb2 import MessageWrapper as MW
from gossip.p2p.p2p_pb2 import IdSpread as IS


class IdSpread(Handler):
    r"""IDSPREAD handler.

    Attributes:
    message_wrapper:    protobuf generated Python class from protobuf spec
    logger:             object for logging
    p2p_input:          queue of p2p messages that are for idspread

    """

    code = MW.IDSPREAD

    def __init__(self, params: dict):
        r"""Construct idspread handler.

        \param  params       dictionary of parameters passed from handlewrapper
        """
        self.message_wrapper = params['message_wrapper']
        self.id_spread = IS()
        self.p2p_input = params['p2p_input']
        self.logger = logging.getLogger(self.__class__.__name__)
        if params['conn']:
            params['conn'].close()

    def parse(self) -> bool:
        """Check formatting."""
        if not self.message_wrapper.HasField('idSpread'):
            self.logger.error(
                'Missing idSpread field')
            return False

        self.id_spread = self.message_wrapper.idSpread

        if len(self.id_spread.peers) == 0:
            self.logger.error(
                'No peers supplied in idSpread')
            return False

        return True

    def action(self) -> bool:
        """Process message."""
        if self.id_spread.type in [IS.PULL_REPLY,
                                   IS.PUSH_REQUEST,
                                   IS.PULL_REQUEST]:
            # Local view has been supplied to me
            self.p2p_input.put(self.message_wrapper)
        else:
            self.logger.error('Unknown id spread type')
            return False

        return True
