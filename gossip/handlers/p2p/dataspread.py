"""DATASPREAD handler."""
import logging
from gossip.handlers.handler import Handler
from gossip.p2p.p2p_pb2 import MessageWrapper as MW
from gossip.p2p.p2p_pb2 import DataSpread as DS
from gossip.util.encoders import gossip_notification
from gossip.util.notifications import Notification


class DataSpread(Handler):
    r"""DATASPREAD handler.

    Attributes:
    message_wrapper:    protobuf generated Python class from protobuf spec
    logger:             object for logging
    data_spread:        object for DataSpread protobuf datatype
    sender:             whom sent us the message (address)
    subscriptions:      list of subscribers to specific data types
    notifications:      list of messages that are sent for validation
    notif_timeout:      amount of time for notification to timeout
    api_output:         API output queue
    gossip:             reference to Gossip thread (for local view)
    p2p_output:         P2P output queue

    """

    code = MW.DATASPREAD

    def __init__(self, params: dict):
        r"""Construct dataspread handler.

        \param  params      dictionary of parameters passed from handlerwrapper
        """
        self.message_wrapper = params['message_wrapper']
        self.my_ip = params['my_ip']
        self.data_spread = DS()
        self.sender = self.message_wrapper.ip
        self.subscriptions = params['subscriptions']
        self.notifications = params['notifications']
        self.notif_timeout = params['timeout']
        self.api_output = params['api_output']
        self.gossip = params['brahms_gossip']
        self.p2p_output = params['p2p_output']
        self.logger = logging.getLogger(self.__class__.__name__)
        if params['conn']:
            params['conn'].close()

    def parse(self) -> bool:
        """Check formatting."""
        if not self.message_wrapper.HasField('dataSpread'):
            self.logger.error(
                'Missing dataSpread field')
            return False
        else:
            self.data_spread = self.message_wrapper.dataSpread

        return True

    def action(self) -> bool:
        """Process message."""
        # decrease TTL
        self.message_wrapper.dataSpread.ttl -= 1

        if self.data_spread.data_type in self.subscriptions:
            # If we don't care about notification due to ttl
            message_id = 65535
            # Add to notifications if we want to eventually forward
            if self.message_wrapper.dataSpread.ttl > 0:
                message_id = self.notifications.id_generator.get()
                data = self.message_wrapper.SerializeToString()
                # notification for notifications list
                notif = Notification(data=data,
                                     timeout_length=self.notif_timeout,
                                     origin=None,  # me?
                                     data_type=self.data_spread.data_type)
                self.notifications[message_id] = notif

            # data bytes for API sending
            message = gossip_notification(data=self.data_spread.data,
                                          id=message_id,
                                          data_type=self.data_spread.data_type)

            self.api_output.put({
                'data': message,
                'targets': self.subscriptions[self.data_spread.data_type]
            })
        else:  # just forward
            self.logger.debug('No modules subscribed - forwarding')
            # remove sender from peers
            peers = list(filter(lambda x: x.ip != self.sender,
                                self.gossip.view[:]))
            if self.message_wrapper.dataSpread.ttl > 0:
                self.p2p_output.put({
                    'data': self.message_wrapper.SerializeToString(),
                    'targets': peers
                })
            else:
                self.logger.info('message is dead (TTL=0)')
                return False

        return True
