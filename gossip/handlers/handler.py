"""Base for handlers."""


class Handler:
    r"""Skeleton class for handling.

    Attributes:
    params:      dictionary of parameters passed into handler
    code:    distinct call code that is used to choose handler
    """

    # api_code is not set, but has to be in order for the code to work
    def __init__(self, params: dict):
        """Save data locally in the handler."""
        self.params = params
        self.code = -1

    def parse(self):
        """Parse data."""
        raise NotImplementedError('Parse function not implemented')

    def action(self):
        """Work on parsed data."""
        raise NotImplementedError('Action function not implemented')
