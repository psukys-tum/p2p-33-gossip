"""Brahms sampler for sampling nodes."""
import logging
import hmac
import threading
from hashlib import sha256
from random import SystemRandom
from string import ascii_uppercase
from gossip.p2p.p2p_pb2 import Peer


class BrahmsSampler:
    """Brahms sampler class.
    Requires locking since both gossip and prober uses sampler simultaneous.

    h: function that hashes provided ids
    q: currently lowest hash value
    """

    def __init__(self, key_size=10):
        r"""Init logger.

        \param key_size     default generated key size
        """
        self.logger = logging.Logger(self.__class__.__name__)
        self.key_size = key_size
        self.key = None
        self.lock = threading.Lock()

    def gen_key(self, key_size):
        r"""Generate a key for hasher.

        \param key_size     size of key to be generated

        \return a random string of key_size length
        """
        chars = SystemRandom().choices(population=ascii_uppercase,
                                       k=key_size)
        return ''.join(chars)

    def init(self):
        """(complying with naming) Initialize hasher, remove lowest has."""
        try:
            self.lock.acquire()
            self.key = self.gen_key(self.key_size)
            self.h = lambda id: hmac.new(key=self.key.encode('utf-8'),
                                         msg=str(id).encode('utf-8'),
                                         digestmod=sha256).hexdigest()
            self.q = None
        finally:
            self.lock.release()

    def next(self, elem: Peer):
        r"""Conditionally populates q with lowest value.

        \param elem     id that is pushed for comparison
        """
        try:
            self.lock.acquire()
            if self.q is None or self.h(elem.ip) < self.h(self.q.ip):
                self.q = elem
        finally:
            self.lock.release()

    def sample(self):
        r"""Get current lowest hash sized sample.

        \return id of node
        """
        try:
            self.lock.acquire()
            return self.q
        finally:
            self.lock.release()
