"""Brahms gossip algorithm."""
import logging
import queue
import random
import time
from datetime import datetime
import socket
import threading
import math
from gossip.p2p.Sampler import BrahmsSampler
from gossip.util.conf import GossipConfig
from gossip.p2p.prober import Prober
from gossip.p2p.p2p_pb2 import MessageWrapper, IdSpread, Peer
from gossip.util import comp_challenge
from Crypto.PublicKey import RSA


class BrahmsGossip(threading.Thread):
    """Brahms gossip class thread.

    Due to rounding of fractions, view list might be up to three peers
    larger than view_size.

    view is right now a list.

    view: peers
    alfa: push amount
    beta: pull amount
    gamma: history amount
    """
    def __init__(self, initial_peers: list,
                 p2p_output: queue.Queue, p2p_input: queue.Queue,
                 config: GossipConfig,
                 view_size=20, sampler_size=20,
                 alfa=0.40, beta=0.40, is_bootstrapper=False):
        r"""Init logger, view and sampler lists.

        Alfa, beta requirements:
        gamma = 1 - alfa - beta
        alfa > 0, beta > 0, gamma > 0

        \param initial_peers initial view from a bootstrapper
        \param p2p_output p2p output queue
        \param p2p_input p2p input queue for all incoming idSpread messages
        \param config program's config
        \param view_size number of peers in view
        \param sampler_size number of samplers
        \param alfa push request parameter
        \param beta pull request parameter
        """
        threading.Thread.__init__(self)
        self.alfa = alfa
        self.beta = beta
        self.gamma = 1 - alfa - beta
        # requirements check
        if self.gamma <= 0 or alfa <= 0 or beta <= 0:
            raise ValueError('Illegal values of alfa or beta in BrahmsGossip')

        self.logger = logging.Logger(self.__class__.__name__)

        self.samplers = [BrahmsSampler() for i in range(sampler_size)]
        for s in self.samplers:
            s.init()

        self.view = initial_peers
        if not self.view and not is_bootstrapper:
            raise ValueError('No initial peers given in BrahmsGossip')

        self.view_size = view_size
        # to not go over view_size when rounding up
        if self.view_size > 3:
            self.view_size -= 3

        self.p2p_output = p2p_output
        self.p2p_input = p2p_input
        self.ip = config.listen_addr
        self.port = config.listen_port
        self.config = config
        self.finish = False
        self.is_bootstrapper = is_bootstrapper

        self._update_sample(self.view)

    def _update_sample(self, view: list):
        r"""Updates samplers with new id:s"""
        for peer in view:
            for s in self.samplers:
                s.next(peer)

    def _rand(self, view, n) -> list:
        r"""Returns n random choices from view

        \param view list to be sampled from
        \param n number of random choices
        """
        try:
            return random.sample(view, n)
        except ValueError:
            # not enough choices in view, give back as much as possible
            return view

    def _get_sample_view(self) -> list:
        r"""Retrieves all id samples from samplers"""
        sample_view = []
        for s in self.samplers:
            sample = s.sample()
            if sample:
                sample_view.append(sample)

        return sample_view

    def _send_push_request(self, peer: Peer):
        r"""Send push requests to all ids in list

        \param peer to send request to
        """
        msg = MessageWrapper()
        msg.type = MessageWrapper.IDSPREAD
        msg.ip = self.ip
        msg.idSpread.type = IdSpread.PUSH_REQUEST

        p = Peer(ip=self.ip, port=self.port)

        curr_timestamp = datetime.utcnow().isoformat('T') + 'Z'
        msg.timestamp = curr_timestamp

        # fulfill challenge
        msg.pub_key = self.config.public_key.exportKey('DER')

        data = bytes(peer.ip.encode('ascii') +
                     str(peer.port).encode('ascii') +
                     curr_timestamp.encode('ascii'))
        checksum = comp_challenge.hashsum(data=data)
        msg.challenge = comp_challenge.sign(self.config.private_key,
                                            checksum)

        msg.idSpread.peers.extend([p])

        curr_timestamp = datetime.utcnow().isoformat('T') + 'Z'
        msg.timestamp = curr_timestamp

        # fulfill challenge
        msg.pub_key = self.config.public_key.exportKey('DER')

        data = bytes(peer.ip.encode('ascii') +
                     str(peer.port).encode('ascii') +
                     curr_timestamp.encode('ascii'))
        checksum = comp_challenge.hashsum(data=data)
        msg.challenge = comp_challenge.sign(self.config.private_key,
                                            checksum)

        # msg ready to be sent
        data = msg.SerializeToString()
        self.p2p_output.put({'data': data,
                             'targets': [peer]})

    def _send_pull_reply(self, peer: Peer):
        r"""Send pull reply

        \param peer to send reply to
        """
        msg = MessageWrapper()
        msg.type = MessageWrapper.IDSPREAD
        msg.idSpread.type = IdSpread.PULL_REPLY
        msg.ip = self.ip

        # append all peers to reply
        if self.view:
            msg.idSpread.peers.extend(self.view)
        else:
            # nothing to send, might as well quit
            return

        data = msg.SerializeToString()
        # msg ready to be sent
        self.p2p_output.put({'data': data,
                             'targets': [peer]})

    def _send_pull_request(self, peer: Peer):
        r"""Send push request

        \param peer to send request to
        """
        msg = MessageWrapper()
        msg.type = MessageWrapper.IDSPREAD
        msg.idSpread.type = IdSpread.PULL_REQUEST
        msg.ip = self.ip

        p = Peer(ip=self.ip, port=self.port)
        msg.idSpread.peers.extend([p])

        # msg ready to be sent
        data = msg.SerializeToString()
        self.p2p_output.put({'data': data,
                             'targets': [peer]})

    def _extract_peers(self, idSpread: IdSpread) -> list:
        r"""Extracts all attached peers from idSpread message

        \param idSpread holder for peers
        """
        peers = []
        for p in idSpread.peers:
            # check for own id
            if p.ip != self.ip:
                peers.append(p)

        return peers

    def _p2p_input_to_list(self) -> list:
        r"""Extracts available items from input queue."""
        input_list = []
        while True:
            try:
                input_list.append(self.p2p_input.get(block=False))
            except queue.Empty:
                return input_list

    def _handle_msg(self, msg: MessageWrapper,
                    push_view: list, pull_view: list,
                    sent_pull_req: list) -> bool:
        r"""Executes corresponding action to message type

        \param push_view peers we get from push requests
        \param pull_view peers we get from pull replies
        \param sent_pull_req peers we sent pull requests to
        """
        # sanity check
        if msg.type != MessageWrapper.IDSPREAD:
            self.logger.error(
                'BrahmsGossip received wrong msg type\n')
            return False

        if msg.idSpread.type == IdSpread.PUSH_REQUEST:
            peers = self._extract_peers(msg.idSpread)
            if len(peers) == 1 and peers[0].ip != self.ip:
                p = peers[0]
                # Check challenge validity
                pub_key = RSA.importKey(msg.pub_key)
                chal = msg.challenge

                # expected by proto3
                curr = datetime.utcnow()
                # get hashsum of ip+port
                data = bytes(self.ip.encode('ascii') +
                             str(self.port).encode('ascii') +
                             msg.timestamp.encode('ascii'))

                timestamp = datetime.strptime(msg.timestamp,
                                              '%Y-%m-%dT%H:%M:%S.%fZ')

                expected_hashsum = comp_challenge.hashsum(data)

                if comp_challenge.verify(pub_key=pub_key,
                                         thing_to_verify=chal,
                                         expected=expected_hashsum):

                    if abs((curr - timestamp).total_seconds()) < 10:
                        push_view.append(p)
                    else:
                        self.logger.error('Outside acceptable time frame')
                        return False
                else:
                    self.logger.error('Challenge not verified')
                    return False
            else:
                return False

        elif msg.idSpread.type == IdSpread.PULL_REQUEST:
            peers = self._extract_peers(msg.idSpread)
            if len(peers) == 1:
                self._send_pull_reply(peers[0])
            else:
                return False

        elif msg.idSpread.type == IdSpread.PULL_REPLY:
            remove_peer = None
            for peer in sent_pull_req:
                # make sure we sent out a pull request to the peer
                if peer.ip == msg.ip:
                    dup_peers = self._extract_peers(msg.idSpread)
                    # TODO: uniquify not part of Brahms,
                    # maybe remove it for large scale simulations
                    peers = self._uniquify(msg.ip, dup_peers)
                    if peers:
                        pull_view.extend(peers)

                    remove_peer = peer
                    break
            if remove_peer:
                sent_pull_req.remove(peer)
            else:
                return False

        return True

    def _uniquify(self, msg_ip: str, peers: list) -> list:
        r"""Uniquifies a peer list and watches out for self-advertisement

        \param msg_ip ip of sending peer
        \param peers list of (possibly) duplicate peers
        """
        # remove duplicates and don't allow self advertisement
        # can't use set, since peer is not hashable and it is
        # auto-generated, so we can't add it to it.
        # a wrapper in the future perhaps?
        keys = {}
        self_advertisement = False
        for p in peers:
            if msg_ip == p.ip:
                return []
                break
            # don't add ourselves
            elif p.ip != self.ip:
                keys[p.ip] = p

        return list(keys.values())

    def run(self):
        r"""Run the thread doing Gossip loop

        Push and pull requests are sent and handled to update peer view
        """
        # start stale sample invalidation prober
        prober = Prober(self.samplers, frequency=0.5, ip=self.ip)
        prober.start()

        # Make sure we always send out when bootstrapping
        push_amount = math.ceil(self.alfa*self.view_size)
        pull_amount = math.ceil(self.beta*self.view_size)
        history_amount = math.ceil(self.gamma*self.view_size)
        while not self.finish:
            push_view = []
            pull_view = []
            sent_pull_requests = []
            # limited push
            for i in range(min(push_amount, len(self.view))):
                self._send_push_request(self._rand(self.view, 1)[0])

            for i in range(min(pull_amount, len(self.view))):
                peer = self._rand(self.view, 1)[0]
                self._send_pull_request(peer)
                sent_pull_requests.append(peer)

            time.sleep(1)

            # extract all items from queue to get a snapshot of messages
            # instead of processing a continuous neverending flow
            input_list = self._p2p_input_to_list()

            # handle all received push req, pull req and pull reply
            for msg in input_list:
                self._handle_msg(msg, push_view, pull_view, sent_pull_requests)

            # appropriate amount of pushes received
            if len(push_view) <= push_amount and push_view and pull_view:
                tmp = self._rand(push_view, push_amount) + \
                            self._rand(pull_view, pull_amount) + \
                            self._rand(self._get_sample_view(), history_amount)
                # not part of brahms,
                # but good if computation is enough for view size
                self.view = self._uniquify(None, tmp)
                self.logger.info('{}\'s view: {}\n'.
                                 format(self.ip, str(self.view)))

            elif len(self.view) <= 1 and (push_view or pull_view):
                tmp = self._rand(push_view, push_amount) + \
                            self._rand(pull_view, pull_amount) + \
                            self._rand(self._get_sample_view(), history_amount)
                self.view = self._uniquify(None, tmp)
                self.logger.info('{}\'s view: {}\n'.
                                 format(self.ip, str(self.view)))

            self._update_sample(push_view + pull_view)
        # make sure prober shuts down
        prober.finish = True
        prober.join()
