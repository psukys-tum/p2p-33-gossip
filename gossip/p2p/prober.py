"""Stale sample invalidation for Brahms gossip"""
import logging
import threading
import time
import socket
from gossip.p2p.Sampler import BrahmsSampler
from gossip.p2p.p2p_pb2 import MessageWrapper, HelloAuth, Peer
from gossip.util.conf import GossipConfig
from google.protobuf.message import DecodeError


class Prober(threading.Thread):
    r"""Probe class thread."""

    def __init__(self, samplers: list, frequency: int,
                 ip: str):
        r"""Init logger.

        \param samplers samplers used in gossip
        \param frequency frequency of stale sample invalidation in Hz
        """
        threading.Thread.__init__(self)
        self.logger = logging.Logger(self.__class__.__name__)
        self.samplers = samplers
        self.frequency = frequency
        self.ip = ip
        self.finish = False

    def run(self):
        r"""Checks for nonresponding peers in the samples samples"""
        while not self.finish:
            probed_peers = []
            dead_peers = []
            for s in self.samplers:
                peer = s.sample()
                # Don't probe peer if we know it's alive
                if not peer or peer.ip in probed_peers or self.finish:
                    continue
                # Don't probe peer if we know it's dead
                if peer.ip in dead_peers:
                    s.init()
                    continue
                if not self._probe(peer):
                    dead_peers.append(peer.ip)
                    s.init()
                else:
                    probed_peers.append(peer.ip)

            time.sleep(1/self.frequency)

    def _create_hello_msg(self) -> bytes:
        msg = MessageWrapper()
        msg.type = MessageWrapper.HELLOAUTH
        msg.ip = self.ip
        msg.helloAuth.const = HelloAuth.REQUEST
        return msg.SerializeToString()

    def _probe(self, peer: Peer) -> bool:
        r"""Probes peer for its availability through a hello handshake

        \param peer peer to be handshaked with
        """
        s = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        data = self._create_hello_msg()
        s.settimeout(2)
        try:
            s.connect((peer.ip, peer.port))
            s.send(data)
            # this is blocking until timeout
            # payload for hello ~30 bytes
            recv_data = s.recv(256)
        except socket.timeout:
            self.logger.error(
                'No hello answer from {} {}\n'.
                format(peer.ip, str(peer.port)))
            return False
        except OSError:
            self.logger.error(
                'Can\'t connect to {} {}\n'.
                format(peer.ip, str(peer.port)))
            return False

        finally:
            s.close()

        # success receiving data
        recv_msg = MessageWrapper()
        try:
            recv_msg.ParseFromString(recv_data)
        except DecodeError as e:
            self.logger.error(
                'Prober ParseFromString error:\n{0}'.format(e))
            return False

        # right type, const and same id as we think
        if recv_msg.type == MessageWrapper.HELLOAUTH \
           and recv_msg.helloAuth.const == HelloAuth.RESPONSE \
           and recv_msg.ip == peer.ip:
            return True
        else:
            self.logger.error(
                'Wrong hello reply from {} {}\n'.
                format(peer.ip, str(peer.port)))
            return False
