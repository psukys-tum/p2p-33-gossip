"""Universal worker threads rule the grounds here."""
import threading
import queue
import socket
import logging

from gossip import listeners  # used for identifying data in queue
from gossip.handlers.api.handler_wrapper import APIHandlerWrapper
from gossip.handlers.p2p.handler_wrapper import P2PHandlerWrapper
from gossip.util.subscriptions import Subscriptions
from gossip.util.notifications import Notifications
from gossip.p2p.gossip import BrahmsGossip


class Worker(threading.Thread):
    r"""Main class for worker thread logic.

    \param  work_queue      queue for retrieving sockets
    \param  params          dictionary of parameters
    \param  finish          flag for Worker to finish
    """

    def __init__(self, work_queue: queue.Queue,
                 api_output: queue.Queue,
                 p2p_output: queue.Queue,
                 p2p_input: queue.Queue,
                 subscriptions: Subscriptions,
                 notifications: Notifications,
                 brahms_gossip: BrahmsGossip):
        r"""Bootstrap the worker.

        \param  queue   work queue that is shared between the producer
                        (api/p2p listeners) and consumer (worker threads)
        """
        threading.Thread.__init__(self)
        self.work_queue = work_queue
        self.params = {'api_output': api_output,
                       'p2p_output': p2p_output,
                       'p2p_input': p2p_input,
                       'subscriptions': subscriptions,
                       'notifications': notifications,
                       'brahms_gossip': brahms_gossip,
                       'timeout': 1,
                       'my_ip': brahms_gossip.ip}
        self.subscriptions = subscriptions
        self.finish = False
        self.logger = logging.getLogger(self.__class__.__name__)

    def run(self):
        r"""Run the thread - wait for items in queue to process them.

        Makes distinction whether it's a API or P2P call and processes it.
        """
        api_wrapper = APIHandlerWrapper(self.params)
        p2p_wrapper = P2PHandlerWrapper(self.params)
        while not self.finish:
            # This allows us to shutdown without being stuck waiting for item
            try:
                queue_item = self.work_queue.get(timeout=0.5)
            except queue.Empty:
                continue
            conn = queue_item['conn']
            class_type = queue_item['type']
            data = self.read_socket_data(sock=conn)

            if data == b'':
                # socket closed
                if class_type in self.subscriptions:
                    self.subscriptions[class_type].remove(conn)
                conn.close()
                continue
            elif not data:
                # timeout
                self.work_queue.put({'type': class_type, 'conn': conn})
                continue

            self.params['conn'] = conn

            if class_type is listeners.api.APIListener:
                # for now, dismiss the result from handle_message
                api_wrapper.handle_message(data)
                self.work_queue.put({'type': class_type, 'conn': conn})
            elif class_type is listeners.p2p.P2PListener:
                p2p_wrapper.handle_message(data)
            else:
                self.logger.error('Unknown class_type: {0}'.format(class_type))

    def read_socket_data(self, sock: socket.socket, block: int=1024) -> bytes:
        r"""Read all the data that socket intends to send us.

        \param  sock    socket object that is ready to transmit
        \param  block   size of a block that will be read from socket
        \return         data in bytes
        """
        data = bytes()

        try:
            while True:
                segment = sock.recv(block)
                data += segment
                if len(segment) < block:
                    break
        except OSError as err:
            self.logger.info('OS Error: {0}'.format(err))
            return None

        self.logger.debug('Read data (size {0}):\n{1}'.format(len(data), data))
        return data
