import threading
import queue
import time
from gossip.worker import Worker
from gossip.util.subscriptions import Subscriptions
from gossip.util.notifications import Notifications
from gossip.outputters.outputter import Outputter
from gossip.p2p.gossip import BrahmsGossip
from gossip.util.parse_args import parse_args
from gossip.util.conf import GossipConfig
from gossip.util.listeners import create_socket
from gossip.listeners.api import APIListener
from gossip.listeners.p2p import P2PListener
from gossip.p2p.p2p_pb2 import Peer


class Main(threading.Thread):
        """Main thread class."""
        def __init__(self, config_file: str):
                r"""Initiates modules of Gossip

                \param config_file file containing Gossip configuration
                """
                threading.Thread.__init__(self)
                # parse config
                config = GossipConfig(config_file)
                self.config = config
                self.work_queue = queue.Queue()
                self.api_queue = queue.Queue()
                self.p2p_queue = queue.Queue()
                self.id_queue = queue.Queue()
                self.subscriptions = Subscriptions()
                self.notifications = Notifications(config.cache_size)

                api_socket = create_socket(
                        config.api_port, config.api_addr)
                self.api_listener = APIListener(
                        sock=api_socket, queue=self.work_queue)

                p2p_socket = create_socket(
                        config.listen_port, config.listen_addr)
                self.p2p_listener = P2PListener(
                        sock=p2p_socket, queue=self.work_queue)

                self.api_outputter = Outputter(
                        queue=self.api_queue, name='API')
                self.p2p_outputter = Outputter(
                        queue=self.p2p_queue, name='P2P')

                # spawn gossip thread, need a bootstrapper peer list
                initial_peers = []
                if not config.is_bootstrapper:
                        boot_s = Peer(ip=config.bootstrapper_addr,
                                      port=config.bootstrapper_port)
                        initial_peers.append(boot_s)
                max_conn = config.max_connections
                is_bootstrap = config.is_bootstrapper
                self.brahms_gossip = BrahmsGossip(initial_peers,
                                                  view_size=max_conn,
                                                  sampler_size=max_conn,
                                                  config=config,
                                                  p2p_output=self.p2p_queue,
                                                  p2p_input=self.id_queue,
                                                  is_bootstrapper=is_bootstrap)
                # spawn specific amount of threads for working on work_queue
                num_workers = config.num_workers
                self.workers = []
                for _ in range(num_workers):
                        worker = Worker(work_queue=self.work_queue,
                                        api_output=self.api_queue,
                                        p2p_output=self.p2p_queue,
                                        p2p_input=self.id_queue,
                                        subscriptions=self.subscriptions,
                                        notifications=self.notifications,
                                        brahms_gossip=self.brahms_gossip)
                        self.workers.append(worker)

        def run(self):
                """Start Gossip module's threads"""
                self.api_listener.start()
                self.p2p_listener.start()

                self.api_outputter.start()
                self.p2p_outputter.start()

                self.brahms_gossip.start()

                for worker in self.workers:
                        worker.start()

        def clean_up(self):
                """Finish and join all started threads."""
                # trigger exit in all threads.
                for worker in self.workers:
                        worker.finish = True
                self.brahms_gossip.finish = True
                self.api_listener.finish = True
                self.p2p_listener.finish = True
                self.api_outputter.finish = True
                self.p2p_outputter.finish = True

                for worker in self.workers:
                        worker.join()
                self.brahms_gossip.join()
                self.api_listener.join()
                self.p2p_listener.join()
                self.api_outputter.join()
                self.p2p_outputter.join()
