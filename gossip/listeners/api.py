r"""Socket listener for API.

\package gossip.listeners
"""
import logging
import threading
import socket  # type-hinting
import queue  # type-hinting


class APIListener(threading.Thread):
    r"""API socket listener thread class.

    Attributes:
    socket:      socket which will-be/is listened upon
    queue:       queue where received calls are stored for workers
    finish:      flag that APIListener has to stop
    """

    def __init__(self, sock: socket.socket, queue: queue.Queue):
        r"""Initialize API listener.

        \param  sock    set up socket, which can be used to listen to
        \param  queue   thread-safe queue dedicated to store listened API calls
        \param  block   size of block when reading from socket
        """
        threading.Thread.__init__(self)
        self.socket = sock
        self.queue = queue
        self.logger = logging.getLogger(self.__class__.__name__)
        self.finish = False

    def run(self):
        r"""Listen to socket and adds received connections to queue."""
        try:
            while not self.finish:
                # to poll for shutdown
                try:
                    conn, sock_addr = self.socket.accept()
                    conn.settimeout(1)
                except socket.timeout:
                    continue
                self.logger.debug('Accepting from {0}'.format(sock_addr))
                data = ''

                self.queue.put({'type': type(self), 'conn': conn})
        except OSError as err:
            self.logger.error('OS Error: {0}'.format(err))
        finally:
            self.socket.close()
