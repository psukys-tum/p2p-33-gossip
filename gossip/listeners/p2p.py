r"""Socket listener for P2P communication.

\package gossip.listeners
"""
import logging
import threading
import socket
import queue


class P2PListener(threading.Thread):
    r"""P2P socket listener thread class.

    Attributes:
    socket:         socket which will-be/is listened upon
    queue:          queue where received P2P calls are stored for workers
    finish:         flag that P2PListener thread has to stop

    """

    def __init__(self, sock: socket.socket, queue: queue.Queue):
        r"""Initialize P2P listener.

        \param sock     set up socket, which will be used to listen to
        \param queue    thread-safe queue dedicated to store P2P calls
        """
        threading.Thread.__init__(self)
        self.socket = sock
        self.queue = queue
        self.logger = logging.getLogger(self.__class__.__name__)
        self.finish = False

    def run(self):
        """Listen to socket and add received connections to queue."""
        try:
            while not self.finish:
                # to poll for shutdown
                try:
                    conn, sock_addr = self.socket.accept()
                    conn.settimeout(1)
                except socket.timeout:
                    continue
                self.logger.debug('Accepting from {0}'.format(sock_addr))
                self.queue.put({'type': type(self),
                                'conn': conn})
        except OSError as err:
            self.logger.error('OS Error: {0}'.format(err))
        finally:
            self.socket.close()
