r"""Generic outputter.

\package gossip.outputters
"""
import logging
import threading
import socket
import queue
from queue import Queue
from gossip.p2p.p2p_pb2 import Peer


class Outputter(threading.Thread):
    r"""Message outputter thread class.
    Can be provided with sockets or peers in queue key 'targets'

    queue:       output queue which gets populated outside Outputter
    finish:      flag that Outputter has to finish
    name:        name of the Outputter (used for logging)
    """

    def __init__(self, queue: Queue, name: str):
        r"""Initialize outputter.

        \param  queue   queue for messages to output
        \param  name    name of the output instance
        """
        threading.Thread.__init__(self)
        self.queue = queue
        self.logger = logging.Logger(self.__class__.__name__ +
                                     '({0})'.format(name))
        self.finish = False

    def _create_and_connect(self, peer: Peer) -> socket:
        r"""Creates a socket and connects to the peer

        \param peer node we want to connect to
        """
        try:
            s = socket.socket(family=socket.AF_INET,
                              type=socket.SOCK_STREAM)
            s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            s.settimeout(2)
            s.connect((peer.ip, peer.port))
            return s
        except Exception:
            self.logger.error(
                'Can\'t connect to peer {0}'.
                format(str(peer)))
            s.close()
            return None

    def run(self):
        r"""Listen to queue and send messages to sockets."""
        while not self.finish:
            # This allows us to shutdown without being stuck waiting for item
            try:
                item = self.queue.get(timeout=0.5)
            except queue.Empty:
                continue
            data = item['data']
            targets = item['targets']
            if not targets:
                self.logger.debug('Received data without targets')
                continue

            receivers = []
            # spawn sockets and create connections if peers
            if isinstance(targets[0], Peer):
                for peer in targets:
                    s = self._create_and_connect(peer)
                    if s:
                        receivers.append(s)
            # use provided sockets
            elif isinstance(targets[0], socket.socket):
                receivers.extend(targets)

            self.logger.info(
                'Sending data pack (size {0}B) to {1} peer'.format(
                    len(data), len(receivers)))
            for receiver in receivers:
                try:
                    receiver.sendall(data)
                except Exception as err:
                    # documentation does not specify the error
                    self.logger.error('Failed to send data:\n{0}'.format(err))
                finally:
                    if isinstance(targets[0], Peer):
                        receiver.close()
