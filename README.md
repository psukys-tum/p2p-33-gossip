Gossip module implementation
----------------------------
## Status
[![build status](https://gitlab.lrz.de/P2P-33/gossip/badges/master/build.svg)](https://gitlab.lrz.de/P2P-33/gossip/commits/master)
[![coverage report](https://gitlab.lrz.de/P2P-33/gossip/badges/master/coverage.svg)](https://gitlab.lrz.de/P2P-33/gossip/commits/master)

## Documentation
**Online documentation is available at [psukys-tum.gitlab.io/p2p-33-gossip](https://psukys-tum.gitlab.io/p2p-33-gossip)**

Documentation is generated with the use of Doxygen tool.
```
doxygen Doxyfile
```

## Building and Installing
Building and installing the Gossip module:
```
python setup.py build && python setup.py install
```

## Configuration
A configuration file is required to use the Gossip module. The following fields are all required:

* `max_connections` Upper limit of peers we keep track of
* `cache_size` Number of cached data items
* `bootstrapper` Address and port of peer we bootstrap to
* `listen_address` Address and port Gossip module accepts P2P connections to
* `api_address` Address and port Gossip module accepts API connections to
* `num_workers` How many worker threads to use

A sample configuration is available here: [gossip/util/sample.conf](gossip/util/sample.conf).

## Running
Run the Gossip module with:
```
python main.py -c /path/to/config
```

## Testing
Run the Gossip module's tests with:
```
python setup.py test
```

## License
GNU GPLv3
see [LICENSE](LICENSE) for full text.
