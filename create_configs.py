"""Used to create massive amounts of config files

Usage: 'python create_configs.py n'
where n is the number of config files to create.

Configs are generated into testconfigs/
"""
import sys
import os

if len(sys.argv) == 2:
    n = int(sys.argv[1])
else:
    raise ValueError

if not os.path.exists('testconfigs'):
    os.makedirs('testconfigs')

hostkey = "hostkey.pem"
max_conn = 50
cache_size = 100
bootstrapper = "127.0.0.1:5001"
num_workers = 10
lo = 127
api_port_base = 3000
listen_port_base = 5000

created_configs = 0

for i in range(256):
    if created_configs >= n:
        break
    for j in range(256):
        if created_configs >= n:
            break
        for k in range(1, 256):
            if created_configs >= n:
                break
            addr = "{}.{}.{}.{}".format(str(lo), str(i), str(j), str(k))
            api_addr = addr + ":{}".format(str(api_port_base+k))
            listen_addr = addr + ":{}".format(str(listen_port_base+k))
            test_id = k+j*(2**8-1)+i*(2**16-1)
            with open("testconfigs/test{}.conf".
                      format(str(test_id)), "w") as f:
                f.write('[global]\n'
                        'hostkey = {}\n'
                        '\n'
                        '[gossip]\n'
                        'max_connections = {}\n'
                        'cache_size = {}\n'
                        'bootstrapper = {}\n'
                        'listen_address = {}\n'
                        'api_address = {}\n'
                        'num_workers = {}'.
                        format(hostkey,
                               str(max_conn),
                               str(cache_size),
                               bootstrapper,
                               listen_addr,
                               api_addr,
                               str(num_workers)))
            created_configs += 1
