"""Test for computational challenge functions."""
import unittest
from unittest import mock
from gossip.util import comp_challenge
from gossip.util.decoders import decode_pem_with_pub_priv


class TestHashsumFunctions(unittest.TestCase):
    """Tests regarding hashsum functions."""

    def setUp(self):
        """Set-up variables."""
        self.data = [b'hello', b'go', b'sip', b'beer']

    def test_hashsum_type(self):
        """Check that hashsum returns bytes type object."""
        for d in self.data:
            res = comp_challenge.hashsum(d)
            self.assertTrue(isinstance(res, bytes))

    def test_hashsum_consistency(self):
        """Check that hashsum returns same hashes for same input."""
        hashes = {data: comp_challenge.hashsum(data) for data in self.data}
        for d, h in hashes.items():
            self.assertEqual(comp_challenge.hashsum(d), h)


class TestCryptFunctions(unittest.TestCase):
    """Tests regarding cryptographical functions."""

    def setUp(self):
        """Set-up variables."""
        # Such beauty of hardcoded RSA keys
        f = 'hostkey.pem'
        self.private_key, self.public_key = decode_pem_with_pub_priv(f)
        self.data = b'Go Sip Beer'

    def test_sign_type(self):
        """Test the return type for signed message."""
        res = comp_challenge.sign(priv_key=self.private_key,
                                  thing_to_sign=self.data)

        self.assertTrue(isinstance(res, bytes))

    def test_verify_type(self):
        """Test the return type for verified message."""
        res = comp_challenge.verify(pub_key=self.public_key,
                                    thing_to_verify=self.data,
                                    expected=b'')

        self.assertTrue(isinstance(res, bool))

    def test_sign_something(self):
        """Test that signing doesn't return the same data."""
        res = comp_challenge.sign(priv_key=self.private_key,
                                  thing_to_sign=self.data)
        self.assertNotEqual(res, self.data)

    def test_verify_false(self):
        """Test that verification fails on bad data."""
        res = comp_challenge.verify(pub_key=self.public_key,
                                    thing_to_verify=self.data,
                                    expected=b'')
        self.assertFalse(res)

    def test_sign_verify(self):
        """Test that sign and verify work together."""
        enc = comp_challenge.sign(priv_key=self.private_key,
                                  thing_to_sign=self.data)
        dec = comp_challenge.verify(pub_key=self.public_key,
                                    thing_to_verify=enc,
                                    expected=self.data)

        self.assertTrue(dec)
