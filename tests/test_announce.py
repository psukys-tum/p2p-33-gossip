"""Tests Gossip Announce class."""
import unittest
from unittest import mock
import struct
import queue
from gossip.handlers.api.announce import GossipAnnounce
from gossip.util.subscriptions import Subscriptions
from gossip.util.notifications import Notifications
from gossip.p2p.p2p_pb2 import MessageWrapper, Peer
from gossip.p2p.gossip import BrahmsGossip
from gossip.util import conf


class TestGossipAnnounce(unittest.TestCase):
    """Tests regarding gossip announce"""

    def setUp(self):
        """Init before tests"""
        self.p2p_output = queue.Queue()
        self.api_output = queue.Queue()
        self.subscriptions = Subscriptions()
        self.notifications = Notifications(10)
        self.timeout = 1
        self.config = conf.GossipConfig("tests/test.conf")
        self.brahms_gossip = BrahmsGossip(
            [Peer(ip="123.123.123.123", port=123)],
            p2p_output=queue.Queue(), p2p_input=queue.Queue(),
            config=self.config)

        with mock.patch('socket.socket') as mock_socket:
            self.sock = mock_socket
        self.params = {'api_output': self.api_output,
                       'p2p_output': self.p2p_output,
                       'subscriptions': self.subscriptions,
                       'conn': self.sock,
                       'notifications': self.notifications,
                       'brahms_gossip': self.brahms_gossip,
                       'timeout': self.timeout}

    def test_valid_data_with_sub(self):
        """Check that valid data with sub is processed properly"""
        # data should be put into api queue and notification list
        data_type = 1337
        data = struct.pack('!HH', 0, data_type)
        self.params['data'] = data
        self.target = GossipAnnounce(self.params)

        self.assertTrue(self.target.parse())
        with mock.patch('socket.socket') as mock_socket:
            # to make it appear that data_type is subscribed to.
            self.subscriptions[data_type] = [mock_socket]
            self.assertTrue(self.p2p_output.empty())
            self.assertTrue(self.api_output.empty())

            self.target.action()
            # assert correct data has been added to
            # p2p and api queue and notification list
            item = self.api_output.get()
            api_data = item['data']
            api_socks = item['targets']

            try:
                size, msg_type, msg_ip, d_type, msg_data = struct.unpack(
                    "!HHHH{0}s".format(len(api_data)-8), api_data)
            except struct.error:
                self.fail("can't unpack api queue data")

            self.assertEqual(api_socks[0], mock_socket)
            self.assertEqual(data_type, d_type)
            # no data
            self.assertEqual(size, 8)
            self.assertEqual(msg_type, 502)

            self.assertTrue(self.notifications)
            msg_ip2, notif = self.notifications.popitem()
            self.assertEqual(msg_ip, msg_ip2)

            p2p_data = notif.data
            msg = MessageWrapper()
            msg.ParseFromString(p2p_data)

            self.assertEqual(msg.type, MessageWrapper.DATASPREAD)
            self.assertEqual(msg.dataSpread.ttl, 0)
            self.assertEqual(msg.dataSpread.data_type, data_type)
            self.assertEqual(msg.dataSpread.data, bytes())

            self.assertTrue(self.p2p_output.empty())

    def test_valid_data_with_self_sub(self):
        """Check that valid data with self sub is processed properly"""
        # data should be put into the p2p queue
        data_type = 1337
        data = struct.pack('!HH', 0, data_type)
        self.params['data'] = data
        self.target = GossipAnnounce(self.params)

        self.assertTrue(self.target.parse())
        # self subscribe to the data_type.
        self.subscriptions[data_type] = [self.sock]
        self.assertTrue(self.p2p_output.empty())
        self.assertTrue(self.api_output.empty())

        self.target.action()
        # nothing added to notifications
        self.assertFalse(self.notifications)
        # assert correct data in p2p_queue
        item = self.p2p_output.get()
        p2p_data = item['data']
        msg = MessageWrapper()
        msg.ParseFromString(p2p_data)

        self.assertEqual(msg.type, MessageWrapper.DATASPREAD)
        self.assertEqual(msg.dataSpread.ttl, 0)
        self.assertEqual(msg.dataSpread.data_type, data_type)
        self.assertEqual(msg.dataSpread.data, bytes())

        self.assertTrue(self.api_output.empty())

    def test_valid_data_without_sub(self):
        """Check that valid data without subs is processed properly"""
        # data should be put into p2p queue
        data_type = 1337
        data = struct.pack('!HH', 0, data_type)
        self.params['data'] = data
        self.target = GossipAnnounce(self.params)

        self.assertTrue(self.target.parse())
        # make sure queues are empty beforehand
        self.assertTrue(self.p2p_output.empty())
        self.assertTrue(self.api_output.empty())

        self.target.action()
        # nothing added to notifications
        self.assertFalse(self.notifications)
        # assert correct data in p2p_queue
        item = self.p2p_output.get()
        p2p_data = item['data']
        msg = MessageWrapper()
        msg.ParseFromString(p2p_data)

        self.assertEqual(msg.type, MessageWrapper.DATASPREAD)
        self.assertEqual(msg.dataSpread.ttl, 0)
        self.assertEqual(msg.dataSpread.data_type, data_type)
        self.assertEqual(msg.dataSpread.data, bytes())

        self.assertTrue(self.api_output.empty())

    def test_invalid_data(self):
        """Check that invalid msg returns false"""
        # since size is assumed to be correct
        # we should only get errors if len(data) < 4
        invalid_data = bytes()
        self.params['data'] = invalid_data
        self.target = GossipAnnounce(self.params)

        self.assertFalse(self.target.parse())
