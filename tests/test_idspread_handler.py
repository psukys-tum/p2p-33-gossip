"""Tests for IdSpread."""
import unittest
from unittest import mock
from queue import Queue
from gossip.p2p.p2p_pb2 import MessageWrapper as MW
from gossip.p2p.p2p_pb2 import IdSpread as IS
from gossip.handlers.p2p.idspread import IdSpread


class TestIdSpreadHandler(unittest.TestCase):
    """Tests for idspread handler."""

    def setUp(self):
        """Set-up variables."""
        self.params = {
            'message_wrapper': MW(),
            'p2p_input': Queue(),
            'conn': None
        }

    def test_parse_missing_idspread(self):
        """Return false because idSpread is missing."""
        target = IdSpread(params=self.params)
        res = target.parse()
        self.assertFalse(res)

    def test_parse_missing_peers(self):
        """Return false because peers is/are missing."""
        self.params['message_wrapper'].idSpread.type = IS.PUSH_REQUEST
        target = IdSpread(params=self.params)
        res = target.parse()
        self.assertFalse(res)

    def test_parse_normal(self):
        """Run through normal workflow."""
        peer = self.params['message_wrapper'].idSpread.peers.add()
        peer.ip = '123'
        target = IdSpread(params=self.params)
        res = target.parse()
        self.assertTrue(res)

    def test_action_bad_type(self):
        """Return false because of bad spread type."""
        peer = self.params['message_wrapper'].idSpread.peers.add()
        peer.ip = '123'
        self.params['message_wrapper'].idSpread.type = 1337
        target = IdSpread(params=self.params)
        target.parse()
        res = target.action()
        self.assertFalse(res)

    def test_action_normal(self):
        """Run through normal workflow."""
        peer = self.params['message_wrapper'].idSpread.peers.add()
        peer.ip = '123'
        self.params['message_wrapper'].idSpread.type = IS.PUSH_REQUEST
        target = IdSpread(params=self.params)
        target.parse()
        res = target.action()
        self.assertTrue(res)
        self.assertEqual(1, self.params['p2p_input'].qsize())
