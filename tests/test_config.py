"""Tests API listener functionality."""
import unittest
from unittest import mock
from gossip.util import conf
from Crypto.PublicKey import RSA


class TestGossipParser(unittest.TestCase):
    """Tests regarding the config parser."""

    def test_valid_read(self):
        """Test valid read."""
        config = conf.GossipConfig("tests/test.conf")

        self.assertEqual(config.cache_size, 23)
        self.assertEqual(config.max_connections, 13)

        self.assertEqual(config.bootstrapper_addr, '')
        self.assertEqual(config.bootstrapper_port, 2468)

        self.assertEqual(config.listen_addr, '1.2.3.4')
        self.assertEqual(config.listen_port, 3000)

        self.assertEqual(config.api_addr, '5.6.7.8')
        self.assertEqual(config.api_port, 2000)

        self.assertEqual(config.is_bootstrapper, False)

        self.assertEqual(config.num_workers, 999)

        self.assertTrue(isinstance(config.public_key, RSA._RSAobj))
        self.assertTrue(isinstance(config.private_key, RSA._RSAobj))

    def test_valid_bootstrapper(self):
        """Test valid bootstrapper."""
        config = conf.GossipConfig("tests/test_bootstrapper.conf")
        self.assertEqual(config.is_bootstrapper, True)

    def test_invalid_read(self):
        """Test invalid read."""
        # Raised exception
        self.assertRaises(KeyError, conf.GossipConfig, "tests/testerror.conf")
