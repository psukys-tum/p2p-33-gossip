"""Tests for Handler skeleton."""
import unittest
from unittest import mock
from queue import Queue
from gossip.handlers.api.validate import GossipValidate
from gossip.p2p.gossip import BrahmsGossip
from gossip.p2p.p2p_pb2 import Peer
from gossip.util.conf import GossipConfig


class TestValidationHandler(unittest.TestCase):
    """Tests regarding validation."""

    def get_sample_params(self):
        """Simplifier to build up params."""
        return {
            'data': self.data,
            'p2p_output': self.p2p_output,
            'subscriptions': self.subscriptions,
            'notifications': self.notifications,
            'conn': self.sender,
            'brahms_gossip': self.brahms_gossip
        }

    def mock_notification(self, data):
        """Mock notification structure (expected)."""
        notification_mock = mock.Mock()
        notification_mock.data = data
        notification_mock.ttl = 5
        notification_mock.timeout = 123
        notification_mock.data_type = 123
        notification_mock.origin = 1
        return notification_mock

    def setUp(self):
        """Construct test data and test target."""
        self.data = bytes()
        self.p2p_output = Queue()
        self.subscriptions = {}
        self.notifications = {}
        self.sender = mock.Mock()
        self.conf = mock.patch('gossip.util.conf.GossipConfig')
        self.conf.listen_addr = '123'
        self.conf.listen_port = '1337'
        self.brahms_gossip = BrahmsGossip(
            [Peer(ip="123.123.123.123", port=123),
             Peer(ip="123.123.123.124", port=124)],
            p2p_output=Queue(), p2p_input=Queue(),
            config=self.conf)

    def test_parse_bad_bytesize(self):
        """Test that bad bytes format returns false."""
        params = self.get_sample_params()
        params['data'] = (5).to_bytes(length=99, byteorder='big')
        target = GossipValidate(params=params)
        res = target.parse()
        self.assertFalse(res, 'bad bytesize should return False from parser')

    def test_parse_good_bytesize(self):
        """Test that good byte size return true."""
        params = self.get_sample_params()
        params['data'] = (5).to_bytes(length=4, byteorder='big')
        target = GossipValidate(params=params)
        res = target.parse()
        self.assertTrue(res, 'good bytesize should return True from parser')

    def test_parsed_message_id(self):
        """Test values parsed are correct."""
        params = self.get_sample_params()
        message_id = 123
        data = message_id << 16  # push by two bytes (reserved size)
        params['data'] = data.to_bytes(length=4, byteorder='big')
        target = GossipValidate(params=params)
        res = target.parse()
        self.assertTrue(res)
        self.assertEqual(target.message_id,
                         message_id,
                         'parsed message_id is wrong')

    def test_parsed_valid(self):
        """Test that valid flag is parsed correctly."""
        params = self.get_sample_params()
        data = 1  # valid is 1, invalid - 0
        params['data'] = data.to_bytes(length=4, byteorder='big')
        target = GossipValidate(params=params)
        res = target.parse()
        self.assertTrue(res)
        self.assertTrue(target.valid)

    def test_parsed_invalid(self):
        """Test that valid flag is parsed correctly."""
        params = self.get_sample_params()
        data = 0  # valid is 1, invalid - 0
        params['data'] = data.to_bytes(length=4, byteorder='big')
        target = GossipValidate(params=params)
        res = target.parse()
        self.assertTrue(res)
        self.assertFalse(target.valid)

    def test_action_message_not_in_notifications(self):
        """Test false returned on message being not in notifications."""
        params = self.get_sample_params()
        data = 123 << 16
        params['data'] = data.to_bytes(length=4, byteorder='big')
        target = GossipValidate(params=params)
        target.parse()
        res = target.action()
        self.assertFalse(res, 'message should\'t be in notifications')

    def test_action_datatype_not_in_subscriptions(self):
        """Test false returned on message being not in notifications."""
        params = self.get_sample_params()
        data = 123 << 16
        params['data'] = data.to_bytes(length=4, byteorder='big')
        notification_mock = self.mock_notification(data)
        params['notifications'][123] = notification_mock
        params['subscriptions'][1337] = []
        target = GossipValidate(params=params)
        target.parse()
        res = target.action()
        self.assertFalse(res, 'message\'s data_type shouldnot be in subs')

    def test_action_sender_not_in_subscribers(self):
        """Test false returned on sender not in subscribers."""
        params = self.get_sample_params()
        data = 123 << 16
        params['data'] = data.to_bytes(length=4, byteorder='big')
        notification_mock = self.mock_notification(data)

        params['notifications'][123] = notification_mock
        params['subscriptions'][notification_mock.data_type] = []
        params['conn'] = 'abc'
        target = GossipValidate(params=params)
        target.parse()
        res = target.action()
        self.assertFalse(res, 'sender should not be in subscriptions')

    def test_action_invalid(self):
        """Test false returned on message valid flag being 0."""
        params = self.get_sample_params()
        data = 123 << 16
        params['data'] = data.to_bytes(length=4, byteorder='big')
        notification_mock = self.mock_notification(data)

        params['notifications'][123] = notification_mock
        params['subscriptions'][notification_mock.data_type] = ['abc']
        params['conn'] = 'abc'
        target = GossipValidate(params=params)
        target.parse()
        res = target.action()
        self.assertFalse(res, 'message should be invalid')

    def test_action_full(self):
        """Test full workflow."""
        params = self.get_sample_params()
        data = 123 << 16
        data += 1
        params['data'] = data.to_bytes(length=4, byteorder='big')
        notification_mock = self.mock_notification(params['data'])

        params['notifications'][123] = notification_mock
        params['subscriptions'][notification_mock.data_type] = ['abc']
        params['conn'] = 'abc'
        target = GossipValidate(params=params)
        target.parse()
        res = target.action()
        self.assertTrue(res, 'action should pass')

        self.assertEqual(self.p2p_output.qsize(), 1)

        item = self.p2p_output.get()

        self.assertFalse(1 in item['targets'])
        self.assertEqual(len(item['targets']), 2)
        self.assertEqual(item['data'], params['data'])

        self.assertFalse(notification_mock in params['notifications'])
        self.assertEqual(len(params['notifications']), 0)
