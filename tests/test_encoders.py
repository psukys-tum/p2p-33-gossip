"""Test on message encoders."""
import unittest
from gossip.util import encoders
import struct


class TestNotificationEncoder(unittest.TestCase):
    """GOSSIP_NOTIFICATION encoder tests."""

    def setUp(self):
        """Initialize some basic vars."""
        self.data = bytes(50)
        self.id = 1337
        self.data_type = 123
        self.api_code = 502
        self.res = encoders.gossip_notification(data=self.data,
                                                id=self.id,
                                                data_type=self.data_type)

    def decode(self, data):
        """Amazing, state of the art, decoder for spec API message format."""
        header_size = 8
        size, code, id, data_type, data = struct.unpack(
            '!HHHH{0}s'.format(len(data) - header_size), data)
        return (size, code, id, data_type, data)

    def test_output(self):
        """Check that the output is binary."""
        self.assertTrue(type(self.res) is bytes)

    def test_size(self):
        """Check that message's defined size is correct."""
        size, _, _, _, _ = self.decode(self.res)
        self.assertEqual(size, len(self.data) + 8)  # 8 bytes for header

    def test_code(self):
        """Check that message's defined code is correct."""
        _, code, _, _, _ = self.decode(self.res)
        self.assertEqual(code, self.api_code)

    def test_message_id(self):
        """Check that message's id is correct."""
        _, _, id, _, _ = self.decode(self.res)
        self.assertEqual(id, self.id)

    def test_data_type(self):
        """Check that message type is correct."""
        _, _, _, data_type, _ = self.decode(self.res)
        self.assertEqual(data_type, self.data_type)

    def test_data(self):
        """Check that set data is correct."""
        _, _, _, _, data = self.decode(self.res)
        self.assertEqual(data, self.data)
