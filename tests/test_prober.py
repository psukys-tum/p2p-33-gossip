"""Tests Brahms Sampler functionality."""
import unittest
from unittest import mock
import queue
import socket
from gossip.p2p.Sampler import BrahmsSampler
from gossip.p2p.prober import Prober
from gossip.p2p.p2p_pb2 import MessageWrapper, HelloAuth, Peer


class TestGossipThread(unittest.TestCase):
    r"""Tests regarding the prober thread.

    probe and run not tested
    """
    def setUp(self):
        r"""Prepare some variables for each test."""
        self.samplers = [BrahmsSampler()]
        self.samplers[0].init()

        self.peer = Peer(ip="123.123.123.123", port=789)
        self.target = Prober(self.samplers, frequency=2, ip="123.123.123.123")

    def test_create_hello_msg(self):
        r"""Test that creation of hello msg is ok"""
        parsed_msg = MessageWrapper()
        hello_msg = self.target._create_hello_msg()
        parsed_msg.ParseFromString(hello_msg)

        self.assertEqual(parsed_msg.type, MessageWrapper.HELLOAUTH)
        self.assertEqual(parsed_msg.ip, "123.123.123.123")
        self.assertEqual(parsed_msg.helloAuth.const, HelloAuth.REQUEST)

    @mock.patch('socket.socket')
    def test_probe_normal(self, mock_socket):
        """Test normal workflow for prober."""
        # Set-up message that will be pushed back through mocked socket
        m = MessageWrapper()
        m.type = MessageWrapper.HELLOAUTH
        m.helloAuth.const = HelloAuth.RESPONSE
        m.ip = self.peer.ip

        ms_instance = mock_socket.return_value
        # don't really care
        ms_instance.recv.return_value = m.SerializeToString()

        res = self.target._probe(self.peer)
        self.assertTrue(res)
        ms_instance.connect.assert_called()
        ms_instance.close.assert_called()

    @mock.patch('socket.socket')
    def test_probe_socket_timeout(self, mock_socket):
        """Test probe when probed socket is timed out."""
        ms_instance = mock_socket.return_value
        # don't really care
        ms_instance.recv.side_effect = socket.timeout('test mocked')

        res = self.target._probe(self.peer)
        self.assertFalse(res)
        ms_instance.connect.assert_called()
        ms_instance.close.assert_called()

    @mock.patch('socket.socket')
    def test_probe_socket_oserror(self, mock_socket):
        """Test probe when probed socket fails to connect."""
        ms_instance = mock_socket.return_value
        # don't really care
        ms_instance.send.side_effect = OSError('test mocked')

        res = self.target._probe(self.peer)
        self.assertFalse(res)
        ms_instance.connect.assert_called()
        ms_instance.close.assert_called()

    @mock.patch('socket.socket')
    def test_probe_msg_parse_fails(self, mock_socket):
        """Test probe with badly formatted msg."""
        ms_instance = mock_socket.return_value
        # don't really care
        ms_instance.recv.return_value = b'pls break'

        res = self.target._probe(self.peer)
        self.assertFalse(res)
        ms_instance.connect.assert_called()
        ms_instance.close.assert_called()
        ms_instance.recv.assert_called()

    @mock.patch('socket.socket')
    def test_probe_msg_bad_format(self, mock_socket):
        """Test probe with bad msg format."""
        m = MessageWrapper()
        m.type = MessageWrapper.IDSPREAD

        ms_instance = mock_socket.return_value
        # don't really care
        ms_instance.recv = mock.Mock(return_value=m.SerializeToString())

        res = self.target._probe(self.peer)
        self.assertFalse(res)
        ms_instance.connect.assert_called()
        ms_instance.close.assert_called

    @mock.patch('gossip.p2p.prober.Prober._probe')
    def test_run(self, mocked_probe_method):
        """Check that dead (None) peers are not probed."""
        mock_sampler = mock.MagicMock()
        mock_sampler.sample.return_value = None
        target = Prober([mock_sampler], 10, '123')
        target.start()
        target.finish = True
        target.join(0.5)
        self.assertFalse(mocked_probe_method.called)
        self.assertFalse(mock_sampler.ip.called)
        self.assertFalse(mock_sampler.init.called)
