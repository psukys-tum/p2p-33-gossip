"""Test for P2P handler wrapper."""
import unittest
from unittest import mock
from gossip.handlers.p2p.handler_wrapper import P2PHandlerWrapper
from queue import Queue
from gossip.p2p.p2p_pb2 import MessageWrapper


class TestP2PHandlerWrapper(unittest.TestCase):
    """Test for P2P handler wrapper class."""

    def setUp(self):
        """Prep for tests."""
        self.params = {'api_output': Queue(),
                       'p2p_output': Queue()}
        self.target = P2PHandlerWrapper(params=self.params)
        self.message_wrapper = MessageWrapper()
        self.handler_mock = mock.Mock()
        self.handler_mock.code = self.message_wrapper.type
        self.handler_mock.return_value.parse = mock.Mock(return_value=None)
        self.handler_mock.return_value.action = mock.Mock(return_value=None)

    def test_register_handlers(self):
        """Test register_handlers functionality."""
        handlers = self.target.register_handlers()
        self.assertEqual(type(handlers), list)

    def test_get_handler(self):
        """Check whether handler is return by p2p code."""
        h1 = mock.Mock()
        h1.code = 123
        h2 = mock.Mock()
        h2.code = 1337
        self.target.handlers = [h1, h2]
        handler = self.target.get_handler(h1.code)
        self.assertEqual(h1, handler)

    def test_handle_message_invalid(self):
        """Test handle_message with invalid data."""
        data = b'pls break'
        res = self.target.handle_message(message=data)
        self.assertFalse(res)

    def test_handle_message_no_handler(self):
        """Test handle_message when there are no handlers."""
        self.message_wrapper.type = 1337
        data = self.message_wrapper.SerializeToString()
        res = self.target.handle_message(message=data)
        self.assertFalse(res)

    def test_handle_message_handler_parse_fail(self):
        """Test handle_message when handler's parser fails."""
        data = self.message_wrapper.SerializeToString()
        self.handler_mock.return_value.parse.return_value = False
        self.handler_mock.return_value.action.return_value = True
        self.target.handlers = [self.handler_mock]
        res = self.target.handle_message(message=data)
        self.assertFalse(res)

    def test_handle_message_handler_action_fail(self):
        """Test handle message with action that returns false."""
        data = self.message_wrapper.SerializeToString()
        self.handler_mock.return_value.parse.return_value = True
        self.handler_mock.return_value.action.return_value = False
        self.target.handlers = [self.handler_mock]
        res = self.target.handle_message(message=data)
        self.assertFalse(res)

    def test_handle_messsage_valid(self):
        """Normal usecase with valid data."""
        data = self.message_wrapper.SerializeToString()
        self.handler_mock.return_value.parse.return_value = True
        self.handler_mock.return_value.action.return_value = True
        self.handler_mock.code = self.message_wrapper.type
        self.target.handlers = [self.handler_mock]
        res = self.target.handle_message(message=data)
        self.assertTrue(res)
