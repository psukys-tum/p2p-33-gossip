"""Tests Brahms Sampler functionality."""
import unittest
from unittest import mock
from datetime import datetime
import queue
import time
from gossip.p2p.Sampler import BrahmsSampler
from gossip.p2p.gossip import BrahmsGossip
from gossip.p2p.prober import Prober
from gossip.util.conf import GossipConfig
from gossip.p2p.p2p_pb2 import MessageWrapper, IdSpread, Peer
from gossip.util.decoders import decode_pem_with_pub_priv
from gossip.util import comp_challenge


class TestGossipThread(unittest.TestCase):
    r"""Tests regarding the gossip thread.

    Send functions and run function not tested yet.
    """

    def setUp(self):
        """Prepare some variables for each test."""
        h = 'hostkey.pem'
        self.private_key, self.public_key = decode_pem_with_pub_priv(h)
        self.p2p_output = queue.Queue()
        self.p2p_input = queue.Queue()
        self.peer = Peer(ip="123.123.123.123", port=789)
        self.peer2 = Peer(ip="123.123.123.124", port=7810)
        self.peer3 = Peer(ip="123.123.123.125", port=7811)
        self.peer4 = Peer(ip="123.123.123.126", port=7812)
        self.config = GossipConfig("tests/test.conf")
        initial_peers = [self.peer]
        self.target = BrahmsGossip(initial_peers, view_size=20, sampler_size=1,
                                   alfa=0.4, beta=0.4,
                                   p2p_output=self.p2p_output,
                                   p2p_input=self.p2p_input,
                                   config=self.config)

    def create_pull_reply(self, peer: Peer, peers: list) -> MessageWrapper:
        """Help function"""
        msg = MessageWrapper()
        msg.type = MessageWrapper.IDSPREAD
        msg.idSpread.type = IdSpread.PULL_REPLY
        msg.ip = peer.ip

        # append all peers to reply
        msg.idSpread.peers.extend(peers)
        return msg

    def create_push_request(self, peer: Peer) -> MessageWrapper:
        """Help function"""
        msg = MessageWrapper()
        msg.type = MessageWrapper.IDSPREAD
        msg.ip = peer.ip
        msg.pub_key = self.public_key.exportKey(format='DER')
        curr_timestamp = datetime.utcnow().isoformat('T') + 'Z'
        msg.timestamp = curr_timestamp
        data = bytes(self.target.ip.encode('ascii') +
                     str(self.target.port).encode('ascii') +
                     curr_timestamp.encode('ascii'))
        checksum = comp_challenge.hashsum(data)
        chal = comp_challenge.sign(priv_key=self.config.private_key,
                                   thing_to_sign=checksum)
        msg.challenge = chal
        msg.idSpread.type = IdSpread.PUSH_REQUEST
        msg.idSpread.peers.extend([peer])
        return msg

    def create_pull_request(self, peer: Peer) -> MessageWrapper:
        """Help function"""
        msg = MessageWrapper()
        msg.type = MessageWrapper.IDSPREAD
        msg.ip = peer.ip
        msg.idSpread.type = IdSpread.PULL_REQUEST
        msg.idSpread.peers.extend([peer])
        return msg

    def test_update_sample(self):
        r"""Test that update_sample runs."""
        # 3 lines of code and functionality is purely based on sampler
        self.target._update_sample(self.target.view)

    def test_rand(self):
        r"""Test that rand use cases works."""
        # normal query
        rand = self.target._rand(self.target.view, 1)
        self.assertTrue(len(rand) == 1)
        self.assertEqual(rand[0], self.peer)

        # larger query than view
        rand = self.target._rand(self.target.view, 42)
        self.assertTrue(len(rand) == 1)
        self.assertEqual(rand[0], self.peer)

    def test_get_sample_view(self):
        r"""Test that getting samples from samplers work."""
        sample_view = self.target._get_sample_view()
        self.assertTrue(len(sample_view) == 1)
        for peer in sample_view:
            self.assertEqual(peer, self.peer)

    def test_send_push_request(self):
        r"""Test that a push request is put into queue."""
        self.assertTrue(self.p2p_output.empty())
        self.target._send_push_request(self.peer)
        # we should have peer and data in queue now
        self.assertFalse(self.p2p_output.empty())
        d = self.p2p_output.get()

        # verify that we send to right peer
        peers = d['targets']
        self.assertEqual(len(peers), 1)
        peer = peers[0]
        self.assertEqual(peer, self.peer)

        # verify that data is a push_request with our test config
        data = d['data']
        msg = MessageWrapper()
        msg.ParseFromString(data)
        self.assertEqual(msg.type, MessageWrapper.IDSPREAD)
        self.assertEqual(msg.idSpread.type, IdSpread.PUSH_REQUEST)
        self.assertEqual(len(msg.idSpread.peers), 1)
        self.assertTrue(msg.challenge is not b'')
        self.assertTrue(msg.pub_key is not b'')
        peer = msg.idSpread.peers[0]
        self.assertEqual(peer.ip, self.target.ip)
        self.assertEqual(peer.port, self.target.port)

    def test_send_pull_request(self):
        r"""Test that a pull request is put into queue."""
        self.assertTrue(self.p2p_output.empty())
        self.assertTrue(len(self.target.view) == 1)
        self.target._send_pull_request(self.peer)
        # we should have peer and data in queue now
        self.assertFalse(self.p2p_output.empty())
        d = self.p2p_output.get()

        # verify that we send to right peer
        peers = d['targets']
        self.assertEqual(len(peers), 1)
        peer = peers[0]
        self.assertEqual(peer, self.peer)

        # verify that data is a pull_request
        data = d['data']
        msg = MessageWrapper()
        msg.ParseFromString(data)
        self.assertEqual(msg.type, MessageWrapper.IDSPREAD)
        self.assertEqual(msg.idSpread.type, IdSpread.PULL_REQUEST)
        self.assertEqual(len(msg.idSpread.peers), 1)
        peer = msg.idSpread.peers[0]
        self.assertEqual(peer.ip, self.target.ip)
        self.assertEqual(peer.port, self.target.port)

    def test_send_pull_reply(self):
        r"""Test that a pull reply is put into queue."""
        self.assertTrue(self.p2p_output.empty())
        self.target.view.append(self.peer)
        self.assertTrue(len(self.target.view) == 2)
        self.target._send_pull_reply(self.peer)
        # we should have peer and data in queue now
        self.assertFalse(self.p2p_output.empty())
        d = self.p2p_output.get()

        # verify that we send to right peer
        peers = d['targets']
        self.assertEqual(len(peers), 1)
        peer = peers[0]
        self.assertEqual(peer, self.peer)

        # verify that data is a pull_request
        data = d['data']
        msg = MessageWrapper()
        msg.ParseFromString(data)
        self.assertEqual(msg.type, MessageWrapper.IDSPREAD)
        self.assertEqual(msg.idSpread.type, IdSpread.PULL_REPLY)
        self.assertEqual(len(msg.idSpread.peers), 2)

    def test_extract_peers(self):
        r"""Test peer extraction from idspread."""
        idSpread = IdSpread()
        for i in range(5):
            p = idSpread.peers.add()
            p.ip = str(i)
            p.port = i
            peers = self.target._extract_peers(idSpread)
        for i in range(4, -1, -1):
            p = peers.pop()
            self.assertEquals(p.ip, str(i))
            self.assertEquals(p.port, i)

        # everything is popped now
        self.assertFalse(peers)

    def test_p2p_input_to_list(self):
        r"""Test extraction of items from input queue"""
        input_list = self.target._p2p_input_to_list()
        self.assertFalse(input_list)

        for i in range(5):
            self.target.p2p_input.put(i)

        input_list = self.target._p2p_input_to_list()

        self.assertEqual(len(input_list), 5)
        for i in range(4, -1, -1):
            d = input_list.pop()
            self.assertEqual(d, i)

    def test_handle_msg_wrong_type(self):
        r"""Test wrong message type."""
        msg = MessageWrapper()
        msg.type = MessageWrapper.DATASPREAD
        self.assertFalse(self.target._handle_msg(msg, [], [], []))

    def test_handle_push_too_many(self):
        r"""Test receiving a push request with too many peers"""
        msg = self.create_push_request(self.peer)
        msg.idSpread.peers.add()
        self.assertFalse(self.target._handle_msg(msg, [], [], []))

    def test_handle_pull_req_too_many(self):
        r"""Test receiving a pull request with too many peers"""
        msg = self.create_pull_request(self.peer)
        msg.idSpread.peers.add()
        self.assertFalse(self.target._handle_msg(msg, [], [], []))

    def test_handle_pull_req_proper(self):
        r"""Test receiving a proper pull request"""
        msg = self.create_pull_request(self.peer)
        self.assertTrue(self.target._handle_msg(msg, [], [], []))

    def test_handle_msg_push_request(self):
        r"""Test a proper push request"""
        push_view = []
        pull_view = []
        sent_pull_requests = []

        msg = self.create_push_request(self.peer2)

        self.assertTrue(self.target._handle_msg(msg, push_view, pull_view,
                                                sent_pull_requests))
        self.assertEqual(len(push_view), 1)
        self.assertEqual(len(pull_view), 0)
        self.assertEqual(push_view[0], self.peer2)

    def test_handle_msg_pull_reply(self):
        r"""Test a proper pull reply"""
        push_view = []
        pull_view = []
        sent_pull_requests = [self.peer, self.peer2]

        msg = self.create_pull_reply(self.peer2, [self.peer, self.peer3])

        self.assertTrue(self.target._handle_msg(msg, push_view, pull_view,
                                                sent_pull_requests))
        self.assertEqual(len(push_view), 0)
        self.assertEqual(len(pull_view), 2)
        self.assertEqual(len(sent_pull_requests), 1)
        self.assertEqual(sent_pull_requests[0], self.peer)

    def test_handle_msg_pull_reply_without_request(self):
        r"""Test a pull reply without having received a pull request"""
        push_view = []
        pull_view = []
        sent_pull_requests = [self.peer, self.peer3]

        msg = self.create_pull_reply(self.peer2, [self.peer, self.peer])

        self.assertFalse(self.target._handle_msg(msg, push_view, pull_view,
                                                 sent_pull_requests))
        self.assertEqual(len(push_view), 0)
        self.assertEqual(len(pull_view), 0)
        self.assertEqual(len(sent_pull_requests), 2)

    def test_illegal_alfa_beta(self):
        """Make sure an exception is raised if bad alfa beta values"""
        self.assertRaises(ValueError, BrahmsGossip,
                          [self.peer], view_size=5, sampler_size=5,
                          alfa=0.6, beta=0.6,
                          p2p_output=self.p2p_output,
                          p2p_input=self.p2p_input,
                          config=self.config)

    def test_no_initial_peers(self):
        """Make sure an exception is raised if we're not a bootstrapper,
        and have an empty peer list
        """
        self.assertRaises(ValueError, BrahmsGossip,
                          [], view_size=5, sampler_size=5,
                          alfa=0.4, beta=0.4,
                          p2p_output=self.p2p_output,
                          p2p_input=self.p2p_input,
                          config=self.config)

    def test_no_initial_peers_bootstrapper(self):
        """Make sure an exception is NOT raised if we're not a bootstrapper,
        and have an empty peer list
        """
        try:
            BrahmsGossip([], view_size=5, sampler_size=5,
                         alfa=0.4, beta=0.4,
                         p2p_output=self.p2p_output,
                         p2p_input=self.p2p_input,
                         config=self.config,
                         is_bootstrapper=True)
        except ValueError:
            self.fail(
                'bootstrapper raised illegal empty initial peers exception')

    @mock.patch('gossip.p2p.prober.Prober.start')
    def test_thread_finish(self, patched_prober):
        """Check if finish flag works well on thread."""
        self.target.finish = True
        self.target.start()
        self.target.join(timeout=0.5)
        assert(not self.target.is_alive())

    @mock.patch('time.sleep', return_value=None)
    @mock.patch('gossip.p2p.prober.Prober.start')
    def test_bootstrap_condition(self, patched_time_sleep, patched_prober):
        """Make sure a single pull_reply is accepted
        if we're bootstrapping
        """
        # create pull reply
        msg = self.create_pull_reply(self.peer, [self.peer2, self.peer3])
        self.p2p_input.put(msg)

        self.assertEqual(len(self.target.view), 1)

        self.target.start()
        # make sure input queue has been processed
        while not self.p2p_input.empty():
            pass
        self.target.finish = True
        self.target.join(timeout=0.5)
        # 2 peers from pull request and 1 from history
        self.assertEqual(len(self.target.view), 3)

    @mock.patch('time.sleep', return_value=None)
    @mock.patch('gossip.p2p.prober.Prober.start')
    def test_no_bootstrap_condition(self, patched_time_sleep, patched_prober):
        """Make sure a single pull_reply is NOT accepted
        if we're bootstrapping
        """
        # create pull reply
        msg = self.create_pull_reply(self.peer, [self.peer2, self.peer3])
        self.p2p_input.put(msg)

        # make sure len(view) > 1
        self.target.view.append(Peer(ip="1337", port=1337))
        self.assertEqual(len(self.target.view), 2)

        self.target.start()
        # make sure input queue has been processed
        while not self.p2p_input.empty():
            pass
        self.target.finish = True
        self.target.join(timeout=0.5)
        # view unchanged
        self.assertEqual(len(self.target.view), 2)

    @mock.patch('time.sleep', return_value=None)
    @mock.patch('gossip.p2p.prober.Prober.start')
    def test_normal_run(self, patched_time_sleep, patched_prober):
        """Make sure a pull_reply and a push request is accepted into view"""
        # create pull reply
        msg = self.create_pull_reply(self.peer, [self.peer2, self.peer3])
        self.p2p_input.put(msg)

        # create push request
        msg = self.create_push_request(self.peer4)
        self.p2p_input.put(msg)

        # make sure len(view) > 1
        # two of self.peer in view to guarantee pull_request for it
        self.target.view.append(self.peer)
        self.assertEqual(len(self.target.view), 2)

        self.target.start()
        # make sure input queue has been processed
        while not self.p2p_input.empty():
            pass
        self.target.finish = True
        self.target.join(timeout=0.5)
        # 1 from push_request, 1 from history, 2 from pull_reply
        self.assertEqual(len(self.target.view), 4)

    @mock.patch('time.sleep', return_value=None)
    @mock.patch('gossip.p2p.prober.Prober.start')
    def test_self_advertisement(self, patched_time_sleep, patched_prober):
        """Make sure a pull_reply and a push request is accepted into view"""
        # create pull reply
        msg = self.create_pull_reply(self.peer, [self.peer, self.peer3])
        self.p2p_input.put(msg)

        # create push request
        msg = self.create_push_request(self.peer)
        self.p2p_input.put(msg)

        # make sure len(view) > 1
        # two of self.peer in view to guarantee pull_request for it
        self.target.view.append(self.peer)
        self.assertEqual(len(self.target.view), 2)

        self.target.start()
        # make sure input queue has been processed
        while not self.p2p_input.empty():
            pass
        self.target.finish = True
        self.target.join(timeout=0.5)
        # unchanged
        self.assertEqual(len(self.target.view), 2)
