"""Tests API functionality."""
import unittest
from unittest import mock
import queue
import time
from gossip.listeners import api


class TestAPIListener(unittest.TestCase):
    """Tests regarding the API listener."""

    def setUp(self):
        """Prepare some variables before each test."""
        self.sock_addr = 'mocked sock_addr'
        self.queue = queue.Queue()
        self.conn = mock.patch('socket.socket')
        self.conn.settimeout = mock.Mock(return_value=None)
        self.mock_socket = mock.patch('socket.socket')
        self.mock_socket.accept = mock.Mock(return_value=[self.conn,
                                                          self.sock_addr])
        self.target = api.APIListener(sock=self.mock_socket, queue=self.queue)

    def test_thread_finish(self):
        """Check if finish flag works well on thread."""
        self.target.finish = True
        self.target.start()
        self.target.join(timeout=0.5)
        self.assertFalse(self.target.is_alive())

    def test_data_conn_queue(self):
        """Check if data sent is actually received."""
        self.target.start()

        # Wait until APIListener does its job
        timeout = 1000
        curr_time = 0
        time_step = 100

        while self.queue.empty() and timeout >= curr_time:
            curr_time += time_step

        self.assertTrue(curr_time <= timeout)

        self.target.finish = True
        self.target.join()

        val = self.queue.get()
        self.assertEqual(val['type'], api.APIListener)
        self.assertEqual(val['conn'], self.conn)

    def test_data_recv_exception(self):
        """Force an OSError exception."""
        self.mock_socket.accept.side_effect = OSError(mock.Mock(status=123),
                                                      'mocked up error')
        self.target.start()
        self.target.join(timeout=0.5)
        self.assertFalse(self.target.is_alive())
