"""Tests for DataSpread."""
import unittest
from unittest import mock
from queue import Queue
from gossip.p2p import p2p_pb2
from gossip.handlers.p2p.dataspread import DataSpread
from gossip.util.subscriptions import Subscriptions
from gossip.util.notifications import Notifications


class TestDataSpreadHandler(unittest.TestCase):
    """Tests for dataspread handler."""

    def setUp(self):
        """Set-up variables."""
        self.gossip = mock.MagicMock()
        peers = [mock.MagicMock() for _ in range(5)]
        for i, peer in enumerate(peers):
            peer.ip = 'some ' + str(i)
        self.gossip.view.__getitem__.return_value = peers
        self.params = {
            'message_wrapper': p2p_pb2.MessageWrapper(),
            'subscriptions': Subscriptions(),
            'notifications': Notifications(10),
            'timeout': 1,
            'my_ip': 'such ip',
            'brahms_gossip': self.gossip,
            'api_output': Queue(),
            'p2p_output': Queue(),
            'conn': None
        }

    def test_parse_missing_field(self):
        """Check that false returned when msg is missing dataSpread."""
        target = DataSpread(params=self.params)
        res = target.parse()
        self.assertFalse(res)

    def test_parse_normal(self):
        """Check that true is returned on normal workflow."""
        self.params['message_wrapper'].dataSpread.ttl = 99
        target = DataSpread(params=self.params)
        res = target.parse()
        self.assertTrue(res)

    def test_action_no_subs(self):
        """Check true returned and other params on no subs."""
        self.params['message_wrapper'].ip = 'some ip'
        self.params['message_wrapper'].dataSpread.ttl = 99
        target = DataSpread(params=self.params)
        target.parse()
        res = target.action()
        self.assertTrue(res)
        item = self.params['p2p_output'].get(block=False)
        self.assertEqual(self.params['brahms_gossip'].view[:], item['targets'])
        m = p2p_pb2.MessageWrapper()
        m.ParseFromString(item['data'])
        self.assertEqual(98, m.dataSpread.ttl)

    def test_action_subs(self):
        """Check true returned and other params with subs."""
        dtype = 1337
        data = b'hello'
        peers = [mock.MagicMock() for i in range(3)]
        peers[0].ip = 'some'
        peers[1].ip = 'other'
        peers[2].ip = 'another'
        self.params['message_wrapper'].ip = peers[0].ip
        self.params['message_wrapper'].dataSpread.ttl = 99
        self.params['message_wrapper'].dataSpread.data_type = dtype
        self.params['message_wrapper'].dataSpread.data = data
        self.params['subscriptions'][dtype] = peers
        target = DataSpread(params=self.params)
        target.parse()
        res = target.action()
        self.assertTrue(res)
        self.assertEqual(1, len(self.params['notifications']))

        output_item = self.params['api_output'].get(block=False)
        self.assertEqual(peers, output_item['targets'])
        self.assertTrue(data in output_item['data'])  # there's also header

    def test_action_ttl_dead(self):
        """Check false return when TTL is low (1 given)."""
        self.params['message_wrapper'].dataSpread.ttl = 1
        self.params['message_wrapper'].dataSpread.data_type = 1337
        self.params['message_wrapper'].dataSpread.data = b''
        target = DataSpread(params=self.params)
        target.parse()
        res = target.action()
        self.assertFalse(res)
