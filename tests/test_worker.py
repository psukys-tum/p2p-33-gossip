"""Tests worker functionality."""
import unittest
from unittest import mock
import queue
from gossip.worker import Worker
from gossip.listeners.api import APIListener
from gossip.listeners.p2p import P2PListener
from gossip.util.subscriptions import Subscriptions
from gossip.util.notifications import Notifications


class TestWorker(unittest.TestCase):
    """Tests regarding the Worker thread."""

    def setUp(self):
        """Prepare some variables before each test."""
        self.work_queue = queue.Queue()
        self.p2p_queue = queue.Queue()
        self.p2p_input = queue.Queue()
        self.api_queue = queue.Queue()
        self.subscriptions = Subscriptions()
        self.notifications = Notifications(10)
        self.mock_socket = mock.patch('socket.socket')
        self.mock_socket.recv = lambda x: b''
        self.brahms_gossip = mock.Mock()
        self.brahms_gossip.ip = '123.123.123.123'
        self.target = Worker(work_queue=self.work_queue,
                             p2p_output=self.p2p_queue,
                             p2p_input=self.p2p_input,
                             api_output=self.api_queue,
                             subscriptions=self.subscriptions,
                             notifications=self.notifications,
                             brahms_gossip=self.brahms_gossip)

    def tearDown(self):
        """Cleanup after the test."""
        # tell thread to finish
        # https://stackoverflow.com/a/323993/552214
        self.target.finish = True

    def test_thread_finish(self):
        """Check if finish flag works well on thread."""
        self.target.finish = True
        self.target.start()
        self.target.join(timeout=0.5)
        assert(not self.target.is_alive())

    def test_socket_read_smaller_than_block(self):
        """Check socket read functionality on data smaller than block."""
        data = [b'1']
        expected = b''.join(data)
        block_size = 2
        self.mock_socket.recv = lambda x: data.pop(0) if data else b''
        self.mock_socket.close = mock.Mock(return_value=None)
        res = self.target.read_socket_data(sock=self.mock_socket,
                                           block=block_size)
        self.assertEqual(res, expected)

    def test_socket_read_equal_to_block(self):
        """Check socket read functionality on data equal to block."""
        data = [b'1', b'2', b'3', b'4', b'5']
        expected = b''.join(data)
        block_size = 1  # data length is only 1
        self.mock_socket.recv = lambda x: data.pop(0) if data else b''
        res = self.target.read_socket_data(sock=self.mock_socket,
                                           block=block_size)
        self.assertEqual(res, expected)

    def test_socket_read_bigger_than_block(self):
        """Check socket read functionality on data bigger than block."""
        data = [b'123', b'321', b'456', b'23', b'199']
        expected = b''.join(data)
        block_size = 2
        self.mock_socket.recv = lambda x: data.pop(0) if data else b''
        self.mock_socket.close = mock.Mock(return_value=None)
        res = self.target.read_socket_data(sock=self.mock_socket,
                                           block=block_size)
        self.assertEqual(res, expected)

    def test_socket_read_oserror(self):
        """Check socket read functionality on data bigger than block."""
        self.mock_socket.recv = mock.Mock()
        self.mock_socket.recv.side_effect = OSError(mock.Mock(status=123))
        res = self.target.read_socket_data(sock=self.mock_socket)
        self.assertEqual(res, None)

    def test_bad_api_message_handle_doesnt_stop(self):
        """Check whether the worker still goes regardless of bad api handle."""
        data = [b'1', b'2', b'3', b'4', b'5']
        self.mock_socket.recv = lambda x: data.pop(0) if data else b''
        self.mock_socket.close = mock.Mock(return_value=None)
        self.work_queue.put({'type': APIListener,
                             'conn': self.mock_socket})
        self.target.start()
        self.target.join(timeout=0.5)  # better solution?
        self.assertTrue(self.target.is_alive())

    def test_bad_p2p_message_handle_doesnt_stop(self):
        """Check whether the worker still goes regardles of bad p2p handle."""
        data = [b'1', b'2', b'3', b'4', b'5']
        self.mock_socket.recv = lambda x: data.pop(0) if data else b''
        self.mock_socket.close = mock.Mock(return_value=None)
        self.work_queue.put({'type': P2PListener,
                             'conn': self.mock_socket})
        self.target.start()
        self.target.join(timeout=0.5)  # better solution?
        self.assertTrue(self.target.is_alive())

    def test_unknown_listener_type_doesnt_stop(self):
        """Test that unknown listener type does not stop the worker."""
        self.mock_socket.close = mock.Mock(return_value=None)
        self.work_queue.put({'type': int,
                             'conn': self.mock_socket})
        self.target.start()
        self.target.join(timeout=0.5)  # better solution?
        self.assertTrue(self.target.is_alive())
