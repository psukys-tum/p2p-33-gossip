"""Tests API functionality."""
import unittest
from unittest import mock
import socket
import time
from datetime import datetime, timedelta
import threading
from gossip.util.notifications import Notifications, Notification
from gossip.util.id_generator import IdGenerator


class TestNotification(unittest.TestCase):
    """Test functionality of notification data struct."""

    def setUp(self):
        """Setup default target Notification."""
        self.data = bytes(123)
        self.timeout_length = 1
        self.data_type = 123
        self.origin = mock.patch('socket.socket')
        self.target = Notification(data=self.data,
                                   data_type=self.data_type,
                                   origin=self.origin,
                                   timeout_length=self.timeout_length)

    def test_setup(self):
        """Check that values passed are ok."""
        self.assertEqual(self.target.data, self.data)
        self.assertEqual(self.target.data_type, self.data_type)

    def test_timeout(self):
        """Check that timeout set is approximately correct."""
        timeout = 5
        target = Notification(data=self.data, origin=self.origin,
                              data_type=self.data_type, timeout_length=5)
        expected_timeout = datetime.now() + timedelta(seconds=timeout)
        # allow variation of 1 second
        self.assertAlmostEqual(target.timeout.timestamp(),
                               expected_timeout.timestamp(),
                               delta=1000)

    def test_is_timed_out_true(self):
        """Check that notification is actually timed out."""
        time.sleep(self.timeout_length)
        self.assertTrue(self.target.is_timed_out(datetime.now()))

    def test_is_timed_out_false(self):
        """Check that notification is not timed out (as expected)."""
        self.assertFalse(self.target.is_timed_out(datetime.now()))


class TestNotifications(unittest.TestCase):
    """Tests on Notifications OrderedDict."""

    def setUp(self):
        """Setup target."""
        self.cache_size = 5
        self.data_type = 123
        self.origin = mock.patch('socket.socket')
        self.target = Notifications(size=self.cache_size)

    def test_setitem_thread(self):
        """Check setitem thread safety."""
        def threaded_setitem(target):
            target[123] = 123

        self.target.lock.acquire()
        thread = threading.Thread(target=threaded_setitem, args=(self.target,))
        thread.start()
        thread.join(timeout=0.5)

        self.assertTrue(thread.is_alive())
        self.assertEqual(len(self.target), 0)
        self.target.lock.release()

        thread.join()
        self.assertEqual(len(self.target), 1)

    def test_setitem_popping(self):
        """Check if item get popped if new one is introduced.
        NOTE: id_generator is not used as it blocks until filled by others
        """
        # fill up target
        for i in range(self.cache_size):
            self.target[i] = i

        self.assertEqual(len(self.target), self.cache_size)

        # push over cache size
        for i in range(self.cache_size):
            self.target[i] = i
        self.assertEqual(len(self.target), self.cache_size)

    def test_remove_outdated_thread(self):
        """Check remote_outdated thread safety."""
        def remove_outdated_thread(target):
            target.remove_outdated()

        timeout = 0
        msg_id = self.target.id_generator.get()
        self.target[msg_id] = Notification(data=None,
                                           origin=self.origin,
                                           data_type=self.data_type,
                                           timeout_length=timeout)

        thread = threading.Thread(target=remove_outdated_thread,
                                  args=(self.target,))

        self.target.lock.acquire()
        thread.start()

        # Check that thread has not finished
        thread.join(timeout=0.5)
        self.assertTrue(thread.is_alive())
        self.target.lock.release()
        thread.join()

        self.assertEqual(len(self.target), 0)

    def test_remove_outdated_none(self):
        """Test outdated notification removal, where none are outdated."""
        for msg_id in range(self.cache_size):
            self.target[msg_id] = Notification(data=None,
                                               origin=self.origin,
                                               data_type=self.data_type,
                                               timeout_length=123)

        self.target.remove_outdated()
        self.assertEqual(len(self.target), self.cache_size)

    def test_remove_outdated_some(self):
        """Test outdated notification removal, where some are outdated."""
        # set -not outdated- notifications
        for i in range(self.cache_size // 2):
            msg_id = self.target.id_generator.get()
            self.target[msg_id] = Notification(data=None,
                                               origin=self.origin,
                                               data_type=self.data_type,
                                               timeout_length=123)
        timeout = 1

        for i in range(self.cache_size // 2, self.cache_size):
            msg_id = self.target.id_generator.get()
            self.target[msg_id] = Notification(data=None,
                                               origin=self.origin,
                                               data_type=self.data_type,
                                               timeout_length=timeout)

        time.sleep(timeout)
        self.target.remove_outdated()

        self.assertEqual(len(self.target), self.cache_size // 2)

    def test_remove_outdated_all(self):
        """Test oudated notification removal, where all are outdated."""
        timeout = 1
        for i in range(self.cache_size):
            msg_id = self.target.id_generator.get()
            self.target[msg_id] = Notification(data=None,
                                               origin=self.origin,
                                               data_type=self.data_type,
                                               timeout_length=timeout)

        time.sleep(timeout)
        self.target.remove_outdated()
        self.assertEqual(len(self.target), 0)

    def test_pop_thread(self):
        """Test pop thread safety."""
        def pop_thread(target, key):
            target.pop(key)

        key = self.target.id_generator.get()
        n = Notification(data=None,
                         origin=self.origin,
                         data_type=0, timeout_length=0)
        self.target[key] = n

        thread = threading.Thread(target=pop_thread,
                                  args=(self.target, key,))

        self.target.lock.acquire()
        thread.start()

        thread.join(0.5)

        self.assertEquals(len(self.target), 1)
        self.target.lock.release()
        thread.join()
        self.assertEquals(len(self.target), 0)

    def test_pop(self):
        """Test that popping specific element works."""
        ids = []
        for i in range(self.cache_size):
            msg_id = self.target.id_generator.get()
            ids.append(msg_id)
            self.target[msg_id] = Notification(data=bytes(msg_id),
                                               origin=self.origin,
                                               data_type=self.data_type,
                                               timeout_length=123)

        for msg_id in ids:
            popped = self.target.pop(msg_id)
            self.assertEqual(popped.data, bytes(msg_id))
