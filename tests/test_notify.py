"""Tests Gossip Notify class."""
import struct
import unittest

from unittest import mock
from gossip.util.subscriptions import Subscriptions
from gossip.handlers.api.notify import GossipNotify


class TestGossipNotify(unittest.TestCase):
    """Tests regarding gossip notify."""

    def setUp(self):
        """Init before tests."""
        self.subscriptions = Subscriptions()
        with mock.patch('socket.socket') as mock_socket:
            self.conn = mock_socket
            self.params = {'subscriptions': self.subscriptions,
                           'conn': self.conn}

    def test_valid_data(self):
        """Check that valid data is added to subscription list."""
        data_type = 1337
        valid_data = struct.pack('!HH', 0, data_type)
        self.params['data'] = valid_data
        self.target = GossipNotify(self.params)

        self.assertTrue(self.target.parse())

        self.target.action()
        self.assertTrue(self.conn in self.subscriptions[data_type])

    def test_valid_data_double_notify(self):
        """Check that subscriber is only added once."""
        data_type = 1337
        valid_data = struct.pack('!HH', 0, data_type)
        self.params['data'] = valid_data
        self.target = GossipNotify(self.params)

        self.assertTrue(self.target.parse())

        self.target.action()
        self.assertTrue(self.conn in self.subscriptions[data_type])
        self.target.action()
        self.assertTrue(self.conn in self.subscriptions[data_type])
        self.assertEqual(len(self.subscriptions[data_type]), 1)

    def test_valid_data_double_distinct(self):
        """Check that multiple distinct sockets can be added."""
        data_type = 1337
        valid_data = struct.pack('!HH', 0, data_type)
        self.params['data'] = valid_data
        self.target = GossipNotify(self.params)

        self.assertTrue(self.target.parse())

        self.target.action()
        self.assertTrue(self.conn in self.subscriptions[data_type])
        with mock.patch('socket.socket') as mock_socket:
            # insert another socket to same data_type
            self.params['conn'] = mock_socket
            self.target2 = GossipNotify(self.params)
            self.assertTrue(self.target2.parse())
            self.target2.action()
            # make sure everything was inserted and distinct
            self.assertTrue(mock_socket in self.subscriptions[data_type])
            self.assertTrue(self.conn in self.subscriptions[data_type])
            self.assertEqual(len(self.subscriptions[data_type]), 2)

    def test_invalid_data(self):
        """Check that invalid msg returns false in parse."""
        # since size is assumed to be correct
        # we should only get errors if len(data) < 4
        invalid_data = bytes()
        self.params['data'] = invalid_data
        self.target = GossipNotify(self.params)

        self.assertFalse(self.target.parse())
