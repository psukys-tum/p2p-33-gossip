"""Tests regarding decoders"""
import unittest
from unittest import mock
import os.path
from gossip.util import decoders


class TestPEMDecode(unittest.TestCase):
    """Tests regarding PEM decoder."""

    def setUp(self):
        """Prepare variables."""
        self.pem_path = 'hostkey.pem'
        # crucial that the file was generated before
        self.assertTrue(os.path.isfile(self.pem_path))

    def test_decode_returns(self):
        """Check that private and public keys are returned in a tuple."""
        res = decoders.decode_pem_with_pub_priv(self.pem_path)
        self.assertEqual(2, len(res))
