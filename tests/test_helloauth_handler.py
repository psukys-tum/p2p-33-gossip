"""Tests for HelloAuth."""
import unittest
from unittest import mock
from queue import Queue
from gossip.p2p import p2p_pb2
from gossip.handlers.p2p.hello import HelloAuth


class TestHelloAuthHandler(unittest.TestCase):
    """Tests for helloauth handler."""

    def setUp(self):
        """Set-up variables."""
        self.params = {
            'message_wrapper': p2p_pb2.MessageWrapper(),
            'my_ip': '1337',
            'p2p_output': Queue(),
            'conn': 'definitely a socket'
        }
        self.params['message_wrapper'].ip = '123.123.123.123'

    def test_field_empty(self):
        """Check parse for no helloAuth field."""
        # By default if not initiated - empty
        target = HelloAuth(self.params)
        res = target.parse()
        self.assertFalse(res)

    def test_field_wrong_const(self):
        """Check that wrong constant returns false."""
        self.params['message_wrapper'].helloAuth.const = 1337
        target = HelloAuth(self.params)
        res = target.parse()
        self.assertFalse(res)

    def test_parse_normal(self):
        """Run the normal workflow."""
        val = p2p_pb2.HelloAuth.REQUEST
        self.params['message_wrapper'].helloAuth.const = val
        target = HelloAuth(self.params)
        res = target.parse()
        self.assertTrue(res)

    def test_action_request(self):
        """Test given that the message is a request."""
        val = p2p_pb2.HelloAuth.REQUEST
        self.params['message_wrapper'].helloAuth.const = val
        target = HelloAuth(self.params)
        target.parse()
        res = target.action()
        self.assertTrue(res)
        # raises exception if nothing's in queue
        item = self.params['p2p_output'].get(block=False)

        self.assertEqual(
            item['targets'][0], self.params['conn'], 'Sender mismatch')

        m = p2p_pb2.MessageWrapper()
        # raises exception if wrong data
        m.ParseFromString(item['data'])
        self.assertEqual(p2p_pb2.MessageWrapper.HELLOAUTH,
                         m.type, 'Wrong message type')
        self.assertEqual(self.params['my_ip'], m.ip, 'Wrong self id')
        self.assertTrue(m.HasField('helloAuth'), 'Message missing helloAuth')
        self.assertEqual(p2p_pb2.HelloAuth.RESPONSE,
                         m.helloAuth.const,
                         'Wrong helloauth type')

    def test_action_response(self):
        """Check that on response type, false is returned."""
        val = p2p_pb2.HelloAuth.RESPONSE
        self.params['message_wrapper'].helloAuth.const = val
        target = HelloAuth(self.params)
        target.parse()
        res = target.action()
        self.assertFalse(res)

    def test_action_response(self):
        """Check that on random (1337) type, false is returned."""
        self.params['message_wrapper'].helloAuth.const = 1337
        target = HelloAuth(self.params)
        target.parse()
        res = target.action()
        self.assertFalse(res)
