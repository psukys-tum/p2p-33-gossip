"""Test/Showcase protobuffer functionality."""
import unittest
from gossip.util.id_generator import IdGenerator


class TestIdGenerator(unittest.TestCase):
    r"""Tests regarding message generator."""

    def test_init(self):
        r"""Test that initialization correctly populates it"""
        size = 1337
        id_gen = IdGenerator(size)
        self.assertEqual(id_gen.qsize(), size)
        self.assertTrue(id_gen.full())
        a = id_gen.get()
        self.assertEqual(id_gen.qsize(), size - 1)
        id_gen.put(a)
        self.assertEqual(id_gen.qsize(), size)

        # check that first 3 are different
        a = id_gen.get()
        b = id_gen.get()
        c = id_gen.get()
        self.assertTrue(a != b)
        self.assertTrue(a != c)
        self.assertTrue(b != c)
