"""Tests Brahms Sampler functionality."""
import unittest
from gossip.p2p.Sampler import BrahmsSampler
from gossip.p2p.p2p_pb2 import Peer


class TestP2pSampler(unittest.TestCase):
    """Tests regarding the Sampler."""

    def setUp(self):
        """Prepare some variables for each test."""
        self.key_size = 15
        self.target = BrahmsSampler(key_size=self.key_size)

    def test_gen_key_length(self):
        """Test that key of specific length is generated."""
        obj = self.target.gen_key(key_size=self.key_size)
        self.assertEqual(len(obj), self.key_size)

    def test_gen_key_chars(self):
        """Test that the key is only of chars."""
        obj = self.target.gen_key(key_size=self.key_size)
        self.assertTrue(obj.isalpha())

    def test_gen_key_randomness(self):
        """Test that the generated key is locally random."""
        iterations = 1000
        keys = []
        for i in range(iterations):
            obj = self.target.gen_key(key_size=self.key_size)
            self.assertFalse(obj in keys, '{0} is not random'.format(obj))
            keys.append(obj)

    def test_init_new_key(self):
        """Test sampler initalizator changes key."""
        old_key = self.target.key  # normally - None
        self.target.init()
        self.assertNotEqual(self.target.key, old_key)

    def test_init_updated_key(self):
        """Test that continuous initalization changes key."""
        iterations = 10
        self.target.init()
        for i in range(iterations):
            old_key = self.target.key
            self.target.init()
            self.assertNotEqual(self.target.key, old_key)

    def test_init_same_hash(self):
        """Test that the has function on the same key returns same has."""
        self.target.init()
        id = 123
        iterations = 10
        old_hash = self.target.h(id)
        for i in range(iterations):
            new_hash = self.target.h(id)
            self.assertEqual(new_hash, old_hash)
            old_hash = new_hash

    def test_init_hash_size(self):
        """Test that hash size is above 160 bits (Brahms requirement)."""
        self.target.init()
        id = 123
        hash = self.target.h(id)
        # Each hexadecimal is 4 bytes
        self.assertTrue(len(hash) * 4 > 160,
                        'Generated hash is not sufficient')

    def test_next(self):
        """Test next updates to lowest value."""
        self.target.init()

        # Replace hasher into direct items
        self.target.h = lambda id: id
        ips = ["6", "5", "1", "2", "3", "4", "2"]
        for ip in ips:
            peer = Peer(ip=ip, port=42)
            self.target.next(peer)
        self.assertEquals(self.target.q.ip, min(ips))

    def test_sample_none(self):
        """Test that initialized sample is None."""
        self.target.init()
        self.assertEqual(None, self.target.sample())

    def test_sample_min(self):
        """Test that sample returns minimal value."""
        self.target.init()

        # Replace hasher into direct items
        self.target.h = lambda id: id
        ips = ["6", "5", "1", "2", "3", "4", "2"]
        for ip in ips:
            peer = Peer(ip=ip, port=42)
            self.target.next(peer)
        self.assertEquals(self.target.sample().ip, min(ips))
