#!/usr/bin/env bash
# Usage: ./run_test.sh n, where n is number of peers to be spawned

n=$1
python create_configs.py $n

for i in $(seq 1 $n); do
    python main.py -c testconfigs/test$i.conf &
    pids[$i]=$!
    echo Spawned process: ${pids[$i]}
    if ! (($i % 5));
    then
       sleep 1
    fi
done
# make sure processes have time to register handler
sleep 20

for i in $(seq 1 $n); do
    echo Killing process: ${pids[$i]}
    kill -2 ${pids[$i]}
done
