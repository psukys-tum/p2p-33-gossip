r"""Main logic for gossip.

\package gossip
"""
from gossip.util.parse_args import parse_args
from gossip.main_class import Main
import signal


def signal_handler(signal, frame):
        print('SIGINT received!\nShutting down gracefully\n')


if __name__ == "__main__":
        # trap sigint signals to allow for Ctrl-C shutdown in a nice way
        signal.signal(signal.SIGINT, signal_handler)

        # parse command line options
        args = parse_args()
        main = Main(args.config)
        main.start()
        # wait for SIGINT to shutdown
        signal.pause()

        main.join()
        main.clean_up()
